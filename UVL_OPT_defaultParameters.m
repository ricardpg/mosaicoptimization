Param.Optim.NumIterations = 0;                           % Number of initial Iterations (0 = Ask user).
Param.Optim.NumMaxPoints = 4;                            % Number of Correspondences for the Point-Match (0 => 4 Corners).
Param.Optim.WeightPointMatches = 1;                      % 1/pixelsize = residual in meter.
Param.Optim.WeightRefPoint = 0;
Param.Optim.WeightCamPosX = 0;
Param.Optim.WeightCamPosY = 0;
Param.Optim.WeightCamPosZ = 0;
Param.Optim.WeightCamOriR = 0;
Param.Optim.WeightCamOriP = 0;
Param.Optim.WeightCamOriY = 0;
Param.Optim.WeightMosaicMatches = 0;
Param.Optim.Normalize = true;                            % Perform variable normalization during Optimization.
Param.Optim.PlotEvolution = true;                        % Plot evolution of the residuals while Optimizing.
Param.Optim.CalibrationFile = 'K.mat';                   % Should contain 'K' 3 x 3 (calibration matrix)
                                                         % If there is no calibration available, a
                                                         % default one will be used [1 0 w/2; 0 1 h/2; 0 0 1];
                                                         % the poses computed will then be wrong
Param.Optim.RefPointData = '';                           % Should contain 'RefPointData' 5 x n (Frame Index, x, y, X, Y).
Param.Optim.MosaicMatchesData = '';                      % Should contain 'MosaicMatchesData' 6 x n (Frame Index, Frame Index', x, y, x', y').
Param.Optim.MaximumStats = 20;                           % Maximum Statistics Shown in the final report.
