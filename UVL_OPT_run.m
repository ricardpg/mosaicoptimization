function MOpt = UVL_OPT_run ( M )

%% load default parameters
UVL_OPT_defaultParameters

%% try load specific parameters for sequence
if exist('UVL_sequenceParameters.m', 'file');
    UVL_sequenceParameters
else
    warning('MATLAB:UVL_OPT_run:Input', 'No sequence specific parameters found.');
end

% -[ Loading Reference Points ]--------------------------------------------
if exist(Param.Optim.RefPointData, 'file');
    load(Param.Optim.RefPointData);                     % Should contain 'RefPointData' 5 x n (Frame Index, x, y, X, Y)
else
    RefPointData = [];
end

% -[ Mosaic Matches ]------------------------------------------------------
if exist(Param.Optim.MosaicMatchesData, 'file');
    load(Param.Optim.MosaicMatchesData);                % Should contain 'MosaicMatchesData' 6 x n (Frame Index, Frame Index', x, y, x', y').
else
    MosaicMatchesData = [];
end

% -[ Converting the GMML ]-------------------------------------------------
disp ( 'Converting source data for the Bundle...' );
[RTs, K, PointMatchesIdx, PointMatchesData, CamPosData, CamOriData, RefPointData] = ...
    UVL_OPT_convertGMMLToBundle ( M, RefPointData, Param.Optim.NumMaxPoints );

% -[ Residual Weights ]----------------------------------------------------
% Point and Match (Same weight for every node)
WPointMatches = Param.Optim.WeightPointMatches;

% Reference (Fiducial) Point Reading (Same weight for every node)
WRefPoint = Param.Optim.WeightRefPoint;

% Position Camera Reading (Each different camera can have it's own weight)
WCamPos = zeros ( 3, size ( CamPosData, 2 ) );
WCamPos(1,:) = Param.Optim.WeightCamPosX;
WCamPos(2,:) = Param.Optim.WeightCamPosY;
WCamPos(3,:) = Param.Optim.WeightCamPosZ;
WCamPos = WCamPos(:);

% Camera Orientation Reading (Each different camera can have it's own weight)
WCamOri = zeros ( 3, size ( CamOriData, 2 ) );
WCamOri(1,:) = Param.Optim.WeightCamOriR;
WCamOri(2,:) = Param.Optim.WeightCamOriP;
WCamOri(3,:) = Param.Optim.WeightCamOriY;
WCamOri = WCamOri(:);

% Mosaic Matches (Same weight for every node)
WMosaicMatches = Param.Optim.WeightMosaicMatches;

% --[ Perform the Global Optimization ]------------------------------------
disp ( 'Running Optimization...' );
[iHw0 wHi0 iHw wHi x xt Jacobian JPattern Residual Residualt Normalization] = ...
     UVL_OPT_optimize ( K, RTs, Param.Optim.NumIterations, WPointMatches, PointMatchesIdx, PointMatchesData, ...
                       WRefPoint, RefPointData, WCamPos, CamPosData, WCamOri, CamOriData, WMosaicMatches, MosaicMatchesData, ...
                       Param.Optim.Normalize, Param.Optim.PlotEvolution );

% --[ Save the Optimized final Mosaic ]------------------------------------
% Convert poses to original frames (Heading, X, Y, Z from World, and R, P, Y from vehicle.
disp ( 'Converting back to source frames...' );
MOpt = UVL_OPT_convertBundleToGMML ( M, wHi, x );

disp ( 'Calculating mosaic size...' );
MOpt = UVL_GMML_computeMosaicSize ( MOpt );





% --[ Look for potential outliers ]----------------------------------------

disp ( 'Look for potential outliers...' );
% Take into account the weights 0
if WPointMatches == 0; PointMatchesData = []; PointMatchesIdx = []; end;
if WRefPoint == 0; RefPointData = []; end;
if WCamPos == 0; CamPosData = []; end;
if WCamOri == 0; CamOriData = []; end;
if WMosaicMatches == 0; MosaicMatchesData = []; end;

% Compute How many residuals there are in each set
[NumPointMatches NumRefPoint NumCamPos NumCamOri NumMosaicMatches OffsPointMatches OffsRefPoint OffsCamPos OffsCamOri OffsMosaicMatches] = ...
    UVL_OPT_calcResOffsets ( size(PointMatchesData), size(PointMatchesIdx), size(RefPointData), size(CamPosData), size(CamOriData), size(MosaicMatchesData) );

% Compute offsets and delete minimization figures
if NumPointMatches > 0;
  ResIdx{1} = OffsPointMatches+0:4:OffsPointMatches+NumPointMatches-1;
  ResIdx{2} = OffsPointMatches+1:4:OffsPointMatches+NumPointMatches-1;
  ResIdx{3} = OffsPointMatches+2:4:OffsPointMatches+NumPointMatches-1;
  ResIdx{4} = OffsPointMatches+3:4:OffsPointMatches+NumPointMatches-1;
end
if NumRefPoint > 0;
  ResIdx{5} = OffsRefPoint+0:2:OffsRefPoint+NumRefPoint-1;
  ResIdx{6} = OffsRefPoint+1:2:OffsRefPoint+NumRefPoint-1;
end
if NumCamPos > 0;
  ResIdx{7} = OffsCamPos+0:3:OffsCamPos+NumCamPos-1;
  ResIdx{8} = OffsCamPos+1:3:OffsCamPos+NumCamPos-1;
  ResIdx{9} = OffsCamPos+2:3:OffsCamPos+NumCamPos-1;
end
if NumCamOri > 0;
  ResIdx{10} = OffsCamOri+0:3:OffsCamOri+NumCamOri-1;
  ResIdx{11} = OffsCamOri+1:3:OffsCamOri+NumCamOri-1;
  ResIdx{12} = OffsCamOri+2:3:OffsCamOri+NumCamOri-1;
end
if NumMosaicMatches > 0;
  ResIdx{13} = OffsMosaicMatches+0:4:OffsMosaicMatches+NumMosaicMatches-1;
  ResIdx{14} = OffsMosaicMatches+1:4:OffsMosaicMatches+NumMosaicMatches-1;
  ResIdx{15} = OffsMosaicMatches+2:4:OffsMosaicMatches+NumMosaicMatches-1;
  ResIdx{16} = OffsMosaicMatches+3:4:OffsMosaicMatches+NumMosaicMatches-1;
end

% To consider outlier ns times stdev from mean: 2 => 95%,  3 => 99.7%
NumStDev{1} = 5;
NumStDev{2} = 5;
NumStDev{3} = 5;
NumStDev{4} = 5;
NumStDev{5} = 3;
NumStDev{6} = 3;
NumStDev{7} = 3;
NumStDev{8} = 3;
NumStDev{9} = 3;
NumStDev{10} = 3;
NumStDev{11} = 3;
NumStDev{12} = 3;
NumStDev{13} = 5;
NumStDev{14} = 5;
NumStDev{15} = 5;
NumStDev{16} = 5;

VarName{1}  = 'x';    VarName{2}  = 'y';     VarName{3}  = 'xp';  VarName{4} = 'yp';
VarName{5}  = 'X';    VarName{6}  = 'Y';
VarName{7}  = 'X';    VarName{8}  = 'Y';     VarName{9}  = 'Z';
VarName{10} = 'Roll'; VarName{11} = 'Pitch'; VarName{12} = 'Yaw';
VarName{13} = 'x';    VarName{14} = 'y';     VarName{15} = 'xp';  VarName{16} = 'yp';

ResType{1}  = 'Point Matches (pix)';       ResType{2}  = ResType{1};  ResType{3}  = ResType{1};  ResType{4} = ResType{1};
ResType{5}  = 'Reference Points (pix)';    ResType{6}  = ResType{5};
ResType{7}  = 'Camera Positions (m)';      ResType{8}  = ResType{7};  ResType{9}  = ResType{7};
ResType{10} = 'Camera Orientations (Rad)'; ResType{11} = ResType{10}; ResType{12} = ResType{10};
ResType{13} = 'Mosaic Matches (pix)';      ResType{14} = ResType{13}; ResType{15} = ResType{13}; ResType{16} = ResType{13};

for i = 1 : 16;
  if ( i >= 1 && i <= 4 && NumPointMatches > 0 );
    SubRes = Residual(ResIdx{i}) ./ WPointMatches;
    m = mean ( SubRes );
    s = std ( SubRes );
    Ix = find ( SubRes < ( m - NumStDev{i} * s ) | SubRes > ( m + NumStDev{i} * s ) );

    if ~isempty ( Ix );
      % Calculate the number of points
      if Param.Optim.NumMaxPoints == 0; NumUsedPoints = 4; else NumUsedPoints = Param.Optim.NumMaxPoints; end;

      % Pair Indices in SubMosaic
      Ixv = fix ( ( Ix - 1 ) / NumUsedPoints );

      % Only unique elements (each overlapping frame has at least 4 correspondences)
      I = unique ( Ixv, 'rows' );

      % Allocate Space
      Nodes = zeros ( size ( I, 1 ), 5 );
      for j = 1 : size ( I, 1 );
        k1 = PointMatchesIdx(1, I(j)+1);
        k2 = PointMatchesIdx(2, I(j)+1);

        % Compute Maximum Absolute Residual and Point Number in the subset of NumMaxPoint per frame
        Ii = I(j) * NumUsedPoints;
        [mx k] = max ( abs ( SubRes(Ii+1 : Ii+NumUsedPoints) ) );

        Nodes(j,:) = [j k1 k2 k+1 SubRes(Ii+k)];
      end
    else
      Nodes = [];
    end

  elseif ( i >= 5 && i <= 6 && NumRefPoint > 0 );
    SubRes = Residual(ResIdx{i}) ./ WRefPoint;
    m = mean ( SubRes );
    s = std ( SubRes );
    Ix = find ( SubRes < ( m - NumStDev{i} * s ) | SubRes > ( m + NumStDev{i} * s ) );

    if ~isempty ( Ix );
      Nodes = [ (1:numel(Ix))'   RefPointData(1,Ix)'   SubRes(Ix)]; 
    else
      Nodes = [];
    end

  elseif ( i >= 7 && i <= 9 && NumCamPos > 0 );
    if isscalar ( WCamPos )
        SubRes = Residual(ResIdx{i}) ./ WCamPos;
    else
        SubRes = Residual(ResIdx{i}) ./ WCamPos(1+i-7:3:NumCamPos);
    end
    m = mean ( SubRes );
    s = std ( SubRes );
    Ix = find ( SubRes < ( m - NumStDev{i} * s ) | SubRes > ( m + NumStDev{i} * s ) );

    if ~isempty ( Ix );
      Nodes = [ (1:numel(Ix))'   CamPosData(1,Ix)'   SubRes(Ix)]; 
    else
      Nodes = [];
    end

  elseif ( i >= 10 && i <= 12 && NumCamOri > 0 );
    if isscalar ( WCamOri )
        SubRes = Residual(ResIdx{i}) ./ WCamOri;
    else
        SubRes = Residual(ResIdx{i}) ./ WCamOri(1+i-10:3:NumCamOri);
    end
    m = mean ( SubRes );
    s = std ( SubRes );
    Ix = find ( SubRes < ( m - NumStDev{i} * s ) | SubRes > ( m + NumStDev{i} * s ) );

    if ~isempty ( Ix );
      Nodes = [ (1:numel(Ix))'   CamOriData(1,Ix)'   SubRes(Ix)];
    else
      Nodes = [];
    end

  elseif ( i >= 13 && i <= 16 && NumMosaicMatches > 0 );
    SubRes = Residual(ResIdx{i}) ./ WMosaicMatches;
    m = mean ( SubRes );
    s = std ( SubRes );
    Ix = find ( SubRes < ( m - NumStDev{i} * s ) | SubRes > ( m + NumStDev{i} * s ) );

    if ~isempty ( Ix );
      Nodes = [ (1:numel(Ix))'  MosaicMatchesData(1,Ix(j))'  MosaicMatchesData(2,Ix(j))'  ones(size(Ix,1),1)  SubRes(Ix)];
    else
      Nodes = [];
    end
  else
    Nodes = [];
  end


  % Display statistics
  if ~isempty ( Nodes );
    fprintf ( 'Frames with %s residual (%s) too high (outlier?)\n', ResType{i}, VarName{i} );
    fprintf ( '  Mean  = %f\n', m );
    fprintf ( '  Stdev = %f\n', s );
    fprintf ( '  Interval = m - %d * s < m < m + %d * s  [%f < %f < %f]\n', NumStDev{i}, NumStDev{i},  m - NumStDev{i} * s, m,  m + NumStDev{i} * s );
    if i <= 4 || i >= 13;
      fprintf(' Index | Node I1 | Node I2 | PM Index | Residual\n');
      fprintf('-------+---------+---------+----------+---------\n');

      % Sort Descending by Residual Magnitude
      [dummy NodesI] = sort(abs(Nodes(:,5)), 'descend');

      [Min Idx] = min ( [ size(Nodes,1) Param.Optim.MaximumStats ] );
      for j = 1 : Min;
          fprintf(' %5d   %7d   %7d     %6d   %10.4f\n', Nodes(NodesI(j),:));
      end
      % Show that the list was cut
      if Idx == 2; fprintf('   ...       ...       ...        ...     ... [List was cut after %d entries]\n', Param.Optim.MaximumStats ); end;
    else
      fprintf(' Index | Node    | Residual\n');
      fprintf('-------+---------+---------\n');

      % Sort Descending by Residual Magnitude
      [dummy NodesI] = sort(abs(Nodes(:,3)), 'descend');
      [Min Idx] = min ( [ size(Nodes,1) Param.Optim.MaximumStats ] );
      for j = 1 : Min;
          fprintf(' %5d   %7d   %10.4f\n', Nodes(NodesI(j),:));
      end
      % Show that the list was cut
      if Idx == 2; fprintf('   ...       ...         ... [List was cut after %d entries]\n', Param.Optim.MaximumStats ); end;
    end
    fprintf('\n');
  end
end
