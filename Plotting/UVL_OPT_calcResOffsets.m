%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate number of residuals and their offsets in the
%                  residual vector.
%
%  File          : UVL_OPT_calcResOffsets.m
%  Date          : 15/07/2007 - 06/04/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_calcResOffsets Calculate the number of residuals and their offsets
%                         in the residual vector.
%
%     Input Parameters:
%      PointMatchesSize: Size of Point Matches Residual matrix.
%      RefPointSize: Size of Reference Point Residual matrix.
%      CamPosSize: Size of Camera Position Residual matrix.
%      CamOriSize: Size of Camera Orientation Residual matrix.
%      MosaicMatchesSize: Size of Mosaic Matches Residual matrix.
%
%     Output Parameters:
%      NumPointMatches: Number of Point Matches residuals.
%      NumRefPoint: Number of Reference Point residuals.
%      NumCamPos: Number of Camera Position residuals.
%      NumCamOri: Number of Camera Orientation residuals.
%      NumMosaicMatches: Number of Mosaic Matches residuals.
%      PointMatchesOffs: Offset of Point Matches in the residual vector.
%      RefPointOffs: Offset of Reference Points in the residual vector.
%      CamPosOffs: Offset of Camera Positions in the residual vector.
%      CamOriOffs: Offset of Camera Orientations in the residual vector.
%      MosaicMatchesOffs: Offset of Mosaic Matches in the residual vector.
%

function [ NumPointMatches NumRefPoint NumCamPos NumCamOri NumMosaicMatches ...
           PointMatchesOffs RefPointOffs CamPosOffs CamOriOffs MosaicMatchesOffs ] = ...
             UVL_OPT_calcResOffsets ( PointMatchesSize, PointMatchesIdxSize, RefPointSize, CamPosSize, CamOriSize, MosaicMatchesSize )
  % Test the input parameters
  error ( nargchk ( 6, 6, nargin ) );
  error ( nargoutchk ( 5, 10, nargout ) );

  if ( ~isempty ( PointMatchesSize ) && length ( PointMatchesSize ) ~= 2 ) || ...
     ( ~isempty ( PointMatchesIdxSize ) && length ( PointMatchesIdxSize ) ~= 2 ) || ...
     ( ~isempty ( RefPointSize ) && length ( RefPointSize ) ~= 2 ) || ...
     ( ~isempty ( CamPosSize ) && length ( CamPosSize ) ~= 2 ) || ...
     ( ~isempty ( CamOriSize ) && length ( CamOriSize ) ~= 2 ) || ...
     ( ~isempty ( MosaicMatchesSize ) && length ( MosaicMatchesSize ) ~= 2 );
    error ( 'MATLAB:UVL_OPT_calcResOffsets:Input', ...
            'Length of input vectors must be 2!\n' );

  end

  % Compute How many residuals there are in each set
  NumPointMatches = PointMatchesSize(1) * PointMatchesSize(2);
  NumRefPoint = 2 * RefPointSize(2);
  NumCamPos = 3 * CamPosSize(2);
  NumCamOri = 3 * CamOriSize(2);
  NumMosaicMatches = 4 * MosaicMatchesSize(2);

  if nargout > 5;
    PointMatchesOffs = 1;
    RefPointOffs = NumPointMatches + 1;
    CamPosOffs = NumPointMatches + NumRefPoint + 1;
    CamOriOffs = NumPointMatches + NumRefPoint + NumCamPos + 1;
    MosaicMatchesOffs = NumPointMatches + NumRefPoint + NumCamPos + NumCamOri + 1;
  end
end
