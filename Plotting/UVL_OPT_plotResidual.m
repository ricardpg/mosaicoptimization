%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Plot the residuals grouped in different figures.
%
%  File          : UVL_OPT_plotResidual.m
%  Date          : 14/07/2007 - 06/04/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_plotResidual Plot the residuals grouped in different figures.
%
%     Input Parameters:
%      Residual: 1xn vector of residuals.
%      WPointMatches: Scalar containing the Weight for Point Matches.
%      WRefPoint: Scalar containing the Weight for Reference Point Readings.
%      WCamPos: (p*3)x1 vector containing the Weights for Camera Position Readings.
%      WCamOri: (q*3)x1 vector containing the Weights for Camera Orientation Readings.
%      WMosaicMatches: Scalar containing the Weight for Mosaic Matches.
%      FigPointMatches: Handle to Point Matches figure.
%      FigRefPoint: Handle to Reference Points figure.
%      FigCamPos: Handle to Camera Positions figure.
%      FigCamOri: Handle to Camera Orientations figure.
%      FigMosaicMatches: Handle to Mosaic Matches figure.
%      NumPointMatches: Number of Point Matches residuals.
%      NumRefPoint: Number of Reference Point residuals.
%      NumCamPos: Number of Camera Position residuals.
%      NumCamOri: Number of Camera Orientation residuals.
%      NumMosaicMatches: Number of Mosaic Matches residuals
%
%     Output Parameters:
%

function UVL_OPT_plotResidual ( Residual, WPointMatches, WRefPoint, WCamPos, WCamOri, WMosaicMatches, ...
                                FigPointMatches, FigRefPoint, ...
                                FigCamPos, FigCamOri, FigMosaicMatches, ...
                                NumPointMatches, NumRefPoint, NumCamPos, NumCamOri, NumMosaicMatches )
  % Test the input parameters
  error ( nargchk ( 16, 16, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  % Plot Residuals for Point-Matches if there is entries
  if NumPointMatches > 0;
    ResPointMatches = Residual(1:NumPointMatches) ./ WPointMatches;
    figure ( FigPointMatches );
    FigPointMatchesAxes = gca ( FigPointMatches );
    cla ( FigPointMatchesAxes );

    % Plot in 4 different colors
    Ida = 1:4:size(ResPointMatches); plot ( ResPointMatches(Ida), 'color', [0.75 0 0] );
    Ida = 2:4:size(ResPointMatches); plot ( ResPointMatches(Ida), 'color', [0 0.75 0] );
    Ida = 3:4:size(ResPointMatches); plot ( ResPointMatches(Ida), 'color', [0 0 0.75] );
    Ida = 4:4:size(ResPointMatches); plot ( ResPointMatches(Ida), 'color', [0.75 0.75 0] );
    legend ( 'x', 'y', 'x''', 'y''', -1 );
  end

  % Plot Residuals for Reference-Point readings if there is entries
  if NumRefPoint > 0;
    ResRefPoint = Residual(1+NumPointMatches:NumPointMatches+NumRefPoint) ./ WRefPoint;
    figure ( FigRefPoint );
    FigRefPointAxes = gca ( FigRefPoint );
    cla ( FigRefPointAxes );

    % Plot in 2 different colors
    Ida = 1:2:size(ResRefPoint); plot ( ResRefPoint(Ida), 'color', [0.75 0 0] );
    Ida = 2:2:size(ResRefPoint); plot ( ResRefPoint(Ida), 'color', [0 0.75 0] );
    legend ( 'X', 'Y', -1 );
  end

  % Plot Residuals for Camera Position readings if there is entries
  if NumCamPos > 0;
    ResCamPos = Residual(1+NumPointMatches+NumRefPoint:NumPointMatches+NumRefPoint+NumCamPos) ./ WCamPos;
    figure ( FigCamPos );
    FigCamPosAxes = gca ( FigCamPos );
    cla ( FigCamPosAxes );

    % Plot in 3 different colors
    Ida = 1:3:size(ResCamPos); plot ( ResCamPos(Ida), 'color', [0.75 0 0] );
    Ida = 2:3:size(ResCamPos); plot ( ResCamPos(Ida), 'color', [0 0.75 0] );
    Ida = 3:3:size(ResCamPos); plot ( ResCamPos(Ida), 'color', [0 0 0.75] );
    legend ( 'X', 'Y', 'Z', -1 );
  end

  % Plot Residuals for Camera Orientation readings if there is entries
  if NumCamOri > 0;
    ResCamOri = Residual(1+NumPointMatches+NumRefPoint+NumCamPos:NumPointMatches+NumRefPoint+NumCamPos+NumCamOri) ./ WCamOri;
    figure ( FigCamOri );
    FigCamOriAxes = gca ( FigCamOri );
    cla ( FigCamOriAxes );

    % Plot in 3 different colors
    Ida = 1:3:size(ResCamOri); plot ( ResCamOri(Ida), 'color', [0.75 0 0] );
    Ida = 2:3:size(ResCamOri); plot ( ResCamOri(Ida), 'color', [0 0.75 0] );
    Ida = 3:3:size(ResCamOri); plot ( ResCamOri(Ida), 'color', [0 0 0.75] );
    legend ( 'Roll', 'Pitch', 'Yaw', -1 );
  end
  
  % Plot Residuals for Mosaic-Matches if there is entries
  if NumMosaicMatches > 0;
    ResMosaicMatches = Residual(1:NumMosaicMatches) ./ WMosaicMatches;
    figure ( FigMosaicMatches );
    FigMosaicMatchesAxes = gca ( FigMosaicMatches );
    cla ( FigMosaicMatchesAxes );

    % Plot in 4 different colors
    Ida = 1:4:size(ResMosaicMatches); plot ( ResMosaicMatches(Ida), 'color', [0.75 0 0] );
    Ida = 2:4:size(ResMosaicMatches); plot ( ResMosaicMatches(Ida), 'color', [0 0.75 0] );
    Ida = 3:4:size(ResMosaicMatches); plot ( ResMosaicMatches(Ida), 'color', [0 0 0.75] );
    Ida = 4:4:size(ResMosaicMatches); plot ( ResMosaicMatches(Ida), 'color', [0.75 0.75 0] );
    legend ( 'x', 'y', 'x''', 'y''', -1 );
  end
end
