%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Create figures to plot residuals.
%
%  File          : UVL_OPT_createResidualFigures.m
%  Date          : 15/07/2007 - 06/04/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_createResidualFigures Create figures to plot residuals.
%
%     Input Parameters:
%      NumPointMatches: Number of Point Matches residuals.
%      NumRefPoint: Number of Referece Point residuals.
%      NumCamPos: Number of Camera Position residuals.
%      NumCamOri: Number of Camera Orientation residuals.
%      NumMosaicMatches: Number of Mosaic Matches residuals.
%
%     Output Parameters:
%      ResPointMatchesFig: Handle to Point Maches figure.
%      ResRefPointFig: Handle to Reference Points figure.
%      ResCamPosFig: Handle to Camera Positions figure.
%      ResCamOriFig: Handle to Camera Orientations figure.
%      ResMosaicMatchesFig: Handle to Mosaic Matches figure.
%

function [ ResPointMatchesFig ResRefPointFig ResCamPosFig ResCamOriFig ResMosaicMatchesFig ] = ...
  UVL_OPT_createResidualFigures ( NumPointMatches, NumRefPoint, NumCamPos, NumCamOri, NumMosaicMatches )

  % Test the input parameters
  error ( nargchk ( 5, 5, nargin ) );
  error ( nargoutchk ( 5, 5, nargout ) );

  % Figure Size
  FigW = 0.5;
  FigH = 0.45;    

  % Create individual figures for each set of residuals.
  if NumPointMatches > 0;
    ResPointMatchesFig = figure; s = 'Point Matches Residuals (pixels)'; set ( ResPointMatchesFig, 'Name', s ); title ( gca ( ResPointMatchesFig ), s );
    hold on;
    set ( ResPointMatchesFig, 'Units', 'normalized' );
    set ( ResPointMatchesFig, 'Position', [0 1-FigH FigW FigH] );
  else
    ResPointMatchesFig = [];
  end

  if NumRefPoint > 0;
    ResRefPointFig = figure; s = 'Reference Point Residuals (pixels)'; set ( ResRefPointFig, 'Name', s ); title ( gca ( ResRefPointFig ), s );
    hold on;
    set ( ResRefPointFig, 'Units', 'normalized' );
    set ( ResRefPointFig, 'Position', [0 0 FigW FigH] );
  else
    ResRefPointFig = [];
  end

  if NumCamPos > 0;
    ResCamPosFig = figure; s = 'Camera Position Reading Residuals (m)'; set ( ResCamPosFig, 'Name', s ); title ( gca ( ResCamPosFig ), s );
    hold on;
    set ( ResCamPosFig, 'Units', 'normalized' );
    set ( ResCamPosFig, 'Position', [1-FigW 1-FigH FigW FigH] );
  else
    ResCamPosFig = [];
  end

  if NumCamOri > 0;
    ResCamOriFig = figure; s = 'Camera Orientation Reading Residuals (rads)'; set ( ResCamOriFig, 'Name', s ); title ( gca ( ResCamOriFig ), s );
    hold on;
    set ( ResCamOriFig, 'Units', 'normalized' );
    set ( ResCamOriFig, 'Position', [1-FigW 0 FigW FigH] );
  else
    ResCamOriFig = [];
  end
  
  if NumMosaicMatches > 0;
    ResMosaicMatchesFig = figure; s = 'Mosaic Matches Residuals (pixels)'; set ( ResMosaicMatchesFig, 'Name', s ); title ( gca ( ResMosaicMatchesFig ), s );
    hold on;
    set ( ResMosaicMatchesFig, 'Units', 'normalized' );
    set ( ResMosaicMatchesFig, 'Position', [0 1-FigH FigW FigH] );
  else
    ResMosaicMatchesFig = [];
  end
end
