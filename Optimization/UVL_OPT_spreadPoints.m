function I = UVL_OPT_spreadPoints(PM, NumCorresp)

    % Number of Nodes
    [l, c] = size(PM);

    if l < 4 || c < NumCorresp;
        error ( 'MATLAB:UVL_OPT_spreadPoints:Input', 'Input PM matrix must be a 4xc matrix (c>=NumCorresp)!' );
    end

    if NumCorresp == c;
        I = 1:NumCorresp;
    else
        XMin = min(PM(1,:));
        XMax = max(PM(1,:));
        YMin = min(PM(2,:));
        YMax = max(PM(2,:));
        XCen = (XMin+XMax)/2;
        YCen = (YMin+YMax)/2;
        XMean = mean(PM(1,:));
        YMean = mean(PM(2,:));
        
        InterestPoints = [XMin YMin;
                          XMax YMax;
                          XMin YMax;
                          XMax YMin;
                          XMean YMean;
                          XMin YCen;
                          XMax YCen;
                          XCen YMin;
                          XCen YMax;
                          XCen YCen
                          ];
      
        NumInterestPoints = min(NumCorresp, 9);
        DistPoints = zeros(NumInterestPoints, c);

        for i = 1 : NumInterestPoints,
            % Select Points or Matches
            Points = PM(1:2,:);
            Points(1,:) = Points(1,:) - InterestPoints(i,1);
            Points(2,:) = Points(2,:) - InterestPoints(i,2);
            Points = Points .* Points;
            Points = sqrt ( sum ( Points, 1 ) );
            DistPoints(i,:) = Points;
        end

        I = zeros(NumCorresp, 1); 

        % Select the points
        for i = 1 : NumCorresp; 
          j = mod ( i-1, NumInterestPoints ) + 1;

          [Val, Idx] = min(DistPoints(j, :));
          % Mark
          DistPoints(:,Idx) = Inf;
          % Index
          I(i) = Idx;
        end
    end
end


