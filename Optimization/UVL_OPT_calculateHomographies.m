%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute the planar homography from the 3D vehicle pose.
%
%  File          : UVL_OPT_calculateHomographies.m
%  Date          : 03/04/2006 - 01/04/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_calculateHomographies Using the camera pose (cRw and wTc)
%                                (Extrinsic Parameters) and K (Intrinsic
%                                Parameters), compute the planar homography (and it's
%                                inverse) that map points between the 2D Mosaic Frame
%                                and every 2D Image Frame induced by the camera position.
%
%     Input Parameters:
%      x: 1x(n*6) variable vector where n is the number of Camera Poses
%         which are parametrized with 6 parameters (vxi, vyi, vzi, X, Y, Z).
%      K: 3x3 Intrinsic Camera Parameters Matrix.
%
%     Output Parameters:
%      iHm: 3x3xn Set of absolute 2D Planar Homographies that
%           transform coordinates in the 2D Mosaic Frame to 2D Image Frame.
%      mHi: 3x3xn Set of absolute 2D Planar Homographies that
%           transform coordinates in the 2D Image Frame to 2D Mosaic Frame.
%

function [iHm mHi] = UVL_OPT_calculateHomographies ( x, K )
  % 6 variables per frame = (vxi, vyi, vzi, X, Y, Z)
  n = length ( x ) / 6;

  % Preallocate Homographies
  iHm = zeros ( 3, 3, n );
  mHi = zeros ( 3, 3, n );

  % Compute Inverse of K
  % K_1 = inv ( K );
  %
  %     | au  0 cx |           | 1/au    0   -cx/au |
  % K = |  0 av cy |    K^-1 = |  0    1/av  -cy/av |
  %     |  0  0  1 |           |  0      0      1   |
  %
  K_1 = zeros ( 3, 3 );
  K_1(3,3) = 1;
  K_1(1,1) = 1.0 / K(1,1);
  K_1(2,2) = 1.0 / K(2,2);
  K_1(1,3) = -K(1,3) / K(1,1);
  K_1(2,3) = -K(2,3) / K(2,2);

  % Index to the current set of variables
  k = 0;
  % Index to the current frame
  f = 1;

  % Each homography has 6 unknowns
  for i = 1 : n;
    % Obtain the Quaternion
    %   6 variables per frame = (vxi, vyi, vzi, X, Y, Z)
    % Vector part of the quaternion
    vxi = x(k+1);
    vyi = x(k+2);
    vzi = x(k+3);
    % Compute scalar part of the quaternion
    wi = sqrt ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi );

    % Convert Quaternion to Rotation Matrix
    cRw = quat2rotmatMx ( [wi vxi vyi vzi] );

    % Reading X, Y, Z variables from the vector
    X = x(k+4);
    Y = x(k+5);
    Z = x(k+6);

    % Translation in Matrix form
    wITc = [1 0 -X; ...
            0 1 -Y; ...
            0 0 -Z];

    % Inverse of the Translation
    wITc_1 = [1 0 -X/Z; ...
              0 1 -Y/Z; ...
              0 0 -1/Z ];

    % Obtain the K
    Kn = K(:,:,f);

    % Homography ( 2D Mosaic -> 2D Image )
    iHm(:,:,i) = Kn * cRw * wITc;
    % Inverse Homography ( 2D Image -> 2D Mosaic )
    mHi(:,:,i) = wITc_1 * cRw' * K_1;
    
    % Next set of Variables corresponding to the following image
    k = k + 6;
    f = f + 1;
  end
end
