%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for Camera Position Readings.
%
%  File          : UVL_OPT_residualsCamPos.m
%  Date          : 03/04/2006 - 20/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_residualsCamPos Calculate the Residuals for Camera Position Readings.
%
%     Input Parameters:
%      x: 1x(n*6) variable vector where n is the number of Camera Poses
%         which are parametrized with 6 parameters (vxi, vyi, vzi, X, Y, Z).
%      CamPosData: mx4 matrix containing in each row, the Image Index corresponding to the reading,
%                  and the X, Y and Z coordinates.
%
%     Output Parameters:
%      r: n*3 vector containing the residuals. It contains the x, y and z
%         residual for each Camera Position Reading.
%

function r = UVL_OPT_residualsCamPos ( x, CamPosData )
  % Residual Vector: Three Elements for each Position Reading
  r = zeros ( size ( CamPosData, 2 ) * 3, 1 );

  % Pointer to the current residual Position Reading
  k = 1;

  % Scan Camera Position Readings
  for j = 1 : size ( CamPosData, 2 );
    % Get the image index
    i = CamPosData (1, j);

    % Residuals
    %   6 variables per frame = (vxi, vyi, vzi, X, Y, Z)
    r(k  ) = CamPosData(2, j) - x((i-1)*6+4);
    r(k+1) = CamPosData(3, j) - x((i-1)*6+5);
    r(k+2) = CamPosData(4, j) - x((i-1)*6+6);

    % Next Camera Position Reading triplet in the residual vector
    k = k + 3;
  end
end
