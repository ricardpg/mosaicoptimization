%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the input data for the Bundle Adjustement
%
%  File          : UVL_OPT_convertGMMLToBundle.m
%  Date          : 03/04/2006 - 02/04/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ConvertDataToBundle Using the GMML mosaic structure and the list of Camera 
%                      readings, the function computes the data structures
%                      needed for the bundle.
%
%     Input Parameters:
%      M: GMML structure representing the mosaic.
%      RefPointData: Matrix of 5xr that contains node, x, y, X and Y representing
%                    the image positions (x,y) of the Reference (Fiducial)
%                    Points (X,Y).
%      NumPoints: Zero to compute the 4 corners using the estimated
%                 homography or the number of point-matches that should be
%                 included. If it is not given the corners are used
%
%     Output Parameters:
%      RTs: Vector that, for each node in the mosaic, contains an structure with
%           the C_R_W (cRw) rotation matrix and the translation vector W_T_C (wTc).
%           The C_R_W matrix is the matrix than can transform coordinates in
%           the 3D World Frame to the 3D Camera Frame and the vector W_T_C is
%           the translation of the 3D Camera Frame from the 3D World Frame.
%      MatchIdxTable: 2xk matrix containing k overlapped image frames. The
%                     matrix will contain for each reading (column): Frame
%                     Index 1 and Frame Index 2.
%      PointMatchesData: 4*NumPointsxk matrix containing k sets of image
%                       correspondences (according to the frames in
%                       MatchIdxTable). For each set, there are NumPoints
%                       points that are matched between the pair of
%                       overlapping frames. For each set and each match,
%                       there are 4 stored values: feature x, y (in image 1)
%                       and correspondence x', y' (in image 2).
%      CamPosData: 4xt matrix containing t Camera Position Readings. The matrix
%                  will contain for each reading (column): Frame Index, X, Y and Z.
%      CamOriData: 4xa matrix containing a angular readings. The matrix
%                  will contain for each reading (column): Frame Index, Roll,
%                  Pitch and Yaw in respect to the 3D Mosaic Frame.
%      RefPointData: 5xl matrix containing l imaged points and it's 2D
%                    coordinate. The matrix will contain for each reading
%                    (row): Frame Index, x, y, X and Y.
%

function [RTs, K, MatchIdxTable, PointMatchesData, CamPosData, CamOriData, RefPointData] = ...
      UVL_OPT_convertGMMLToBundle ( M, RefPointData, NumPoints )
  % Test the input parameters
  error ( nargchk ( 2, 3, nargin ) );
  error ( nargoutchk ( 7, 7, nargout ) );

  if ( nargin < 3 ) || ( nargin >= 3 && NumPoints == 0 );
    NumPoints = 4;
    UseCorners = true;
  else
    UseCorners = false;
  end

  % Count the number of edges in the mosaic to avoid growing arrays
  NumNodes = numel ( M.Nodes );
  NumEdges = 0;
  for no = 1 : NumNodes;
    NumEdges = NumEdges + numel ( M.Nodes(no).Edges );
  end
  
  % Create the Correspondences path to all the datasets and merges
  NbrDatasets = numel(M.Header.Datasets);
  for ds = 1 : NbrDatasets,
    CorrespPath{ds} = [M.Header.Datasets(ds).BasePath '/' M.Header.Datasets(ds).CorrespPath];
  end
  for ds = 1 : numel(M.Header.Merges)
    CorrespPath{NbrDatasets + ds} = [M.Header.Merges(ds).BasePath '/' M.Header.Merges(ds).CorrespPath];
  end

  % Allocate the tables and struct to don't grow it
  MatchIdxTable = zeros ( 2, NumEdges );
  PointMatchesData = zeros ( 4*NumPoints, NumEdges );
  CamPosData = zeros ( 4, NumNodes );
  CamOriData = zeros ( 4, NumNodes );
  K = zeros ( 3, 3, NumNodes );
  RTs_Struc.cRw = zeros ( 3, 3 );
  RTs_Struc.wTc = zeros ( 3, 1 );
  RTs = repmat ( RTs_Struc, 1, NumNodes );


 
  % Compute the Transformation 3D Mosaic Frame -> 3D World Frame: W_X = W_R_M * M_X + W_T_M
  % Mosaic Rotation w.r.t. 3D World Frame
  W_R_M = RotX ( pi );
  % Mosaic Translation w.r.t. 3D World Frame
  % (The mosaic is translated according to the X, Y origin but not in moved in Z).
  W_T_M = [ M.Header.MosaicOrigin.X; M.Header.MosaicOrigin.Y; 0 ];

  % Compute Inverse Transformation 3D World Frame -> 3D Mosaic Frame: M_X = M_R_W * W_X + M_T_W
  M_R_W = W_R_M';
  M_T_W = -M_R_W * W_T_M;

  % Compute the Transformation 3D Camera Frame -> 3D Vehicle Frame: V_X = V_R_C * C_X + V_T_C
  % Camera Rotation w.r.t. 3D Vehicle Frame: Camera X axis is 90� wrt vehicle X axis
  V_R_C = RotZ ( pi/2 );
  
  disp ( 'Writing output tables...' );

  Sel = zeros ( 4, NumPoints );
  % Build PointMatchesData and Initial Homographies
  k = 0;
  for no = 1 : NumNodes;
    ds = M.Nodes(no).DatasetIndex;
    for edI = 1 : numel ( M.Nodes(no).Edges );
      k = k + 1;
      % Table containing the indices for the Current and Reference Frames
      MatchIdxTable(1, k) = no;                                            % Current
      MatchIdxTable(2, k) = M.Nodes(no).Edges(edI).NodeIndex;               % Reference
      
      if UseCorners;
        % Compute the Matches in the Edge Pointed Node
        Height = M.Header.Datasets(ds).ImageSize.Height;
        Width  = M.Header.Datasets(ds).ImageSize.Width;
        p = [0 Width 0 Width; 0 0 Height Height; 1 1 1 1];
        m = M.Nodes(no).Edges(edI).Homo.Matrix * p;
        Sel(1:2,:) = p(1:2,:);                             % Current Image
        Sel(3,:) = m(1,:) ./ m(3,:);                       % Reference Image
        Sel(4,:) = m(2,:) ./ m(3,:);
        PointMatchesData(:,k) = Sel(:);
      else
        cp = M.Nodes(no).Edges(edI).MergeIndex;
        if cp == 0, cp = ds; else cp = cp + NbrDatasets; end
        Corresp = UVL_GMML_loadCorrespondenceFile( [CorrespPath{cp} '/' M.Nodes(no).Edges(edI).Corresp] );
        Idx = UVL_OPT_spreadPoints ( Corresp(1:4,:), NumPoints );
        Sel = Corresp(1:4,Idx);                                   % Current Image
        PointMatchesData(:,k) = Sel(:);                         % Reference Image
      end
    end    

    Roll  = M.Nodes(no).Pose(5);
    Pitch = M.Nodes(no).Pose(6);
    Yaw   = M.Nodes(no).Pose(7);
        
    % Vehicle Rotation w.r.t. 3D World Frame (Computed allways in mobile axis: post-multiplication)
    %   Rotation in X (pi): Point the Z axis down (Vehicle frame is like that).
    %                       As yaw is a clockwise angle, now is rotation in the right direction.
    %   Rotation in Z (Yaw-pi/2): First rotation with respect to the heading angle.
    %                             -pi/2 because the heading is according to the north.
    %   Rotation in Y (Pitch): Rotation of the Vehicle in Y direction according to the gravity vector.
    %   Rotation in X (Roll): Rotation of the Vehicle in X direction according to the gravity vector.
    W_R_V = RotX(pi) * RotZ(Yaw - pi/2) * RotY(Pitch) * RotX(Roll);
    
    % Vehicle Translation w.r.t. 3D World Frame
    W_T_V = [ M.Nodes(no).Pose(1); M.Nodes(no).Pose(2); M.Nodes(no).Pose(3) ];

    % Convert a Pose (V_T_C, V_R_C) w.r.t. 3D Vehicle Frame to the 3D World Frame
    % This new pose will also be the Transformation 3D Camera Frame -> 3D World Frame: W_X = W_R_C * C_X + W_T_C
    W_R_C = W_R_V * V_R_C;
    W_T_C = W_T_V;
    
    % Convert a Pose (W_T_C, W_R_C) w.r.t. 3D Camera Frame to the 3D Mosaic Frame
    % This new pose will also be the Trasformation 3D Camera Frame -> 3D Mosaic Frame: M_X = M_R_C * C_X + M_T_C
    M_R_C = M_R_W * W_R_C;
    M_T_C = M_R_W * W_T_C + M_T_W;

    % Store the information
    RTs(no).cRw = M_R_C';
    RTs(no).wTc = M_T_C;


    CamPosData(1,no) = no;
    CamPosData(2,no) = M_T_C(1);  % X
    CamPosData(3,no) = M_T_C(2);  % Y
    CamPosData(4,no) = M_T_C(3);  % Z
    
    
    [R, P, Y] = RotMat2MobileRPY ( M_R_C' );
    CamOriData(1,no) = no;
    CamOriData(2,no) = R;    % Roll
    CamOriData(3,no) = P;    % Pitch
    CamOriData(4,no) = Y;    % Yaw
    
    if isempty (M.Header.Datasets(ds).K),
        error ( 'MATLAB:UVL_OPT_convertGMMLToBundle', 'Dataset n�%d Does not have a K matrix!', ds );
    end
    K(:,:,no) = M.Header.Datasets(ds).K;
    
    if mod(no, 1000) == 0, fprintf('  Current node conversion %d\n', no); end
  end
  
  % Convert Reference Points to Mosaic Frame
  % Just use the x,y and discarting z (which is equal to 0 in the Mosaic Frame)
  M_R_W2 = M_R_W(1:2,1:2);
  M_T_W2 = M_T_W(1:2,:);
  for i = 1 : size ( RefPointData, 2 );
     % WorldPoint = M_R_W * [ RefPointData(4:5,i) 0 ]' + M_T_W;
     % RefPointData(4:5,i) = WorldPoint(1,2)';
     RefPointData(4:5,i) = M_R_W2 * RefPointData(4:5,i) + M_T_W2;
  end  
end
