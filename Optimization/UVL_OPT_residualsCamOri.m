%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for Camera Orientation Readings.
%
%  File          : UVL_OPT_residualsCamOri.m
%  Date          : 03/04/2006 - 20/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_residualsCamOri Calculate the Residuals for Camera Orientation Readings.
%
%     Input Parameters:
%      x: 1x(n*6) variable vector where n is the number of Camera Poses
%         which are parametrized with 6 parameters (vxi, vyi, vzi, X, Y, Z).
%      CamOriData: mx4 Matrix containing in each row the Image Index corresponding to 
%                  the reading and Roll, Pitch and the Yaw Angle.
%                  NOTE: Angles must be in the interval [-pi, pi].
%
%     Output Parameters:
%      r: k*3 vector containing the residuals. It contains the Roll, Pitch
%         and Yaw residual for each set of Camera Angle Readings.
%

function r = UVL_OPT_residualsCamOri ( x, CamOriData )
  % Residual Vector: Three Elements for each Angle Reading
  r = zeros ( size ( CamOriData, 2 ) * 3, 1 );

  % Pointer to the current residual Angle Reading
  k = 1;

  % Scan Angle Readings
  for j = 1 : size ( CamOriData, 2 );
    % Get the image index
    i = CamOriData(1, j);
    % Obtain the Quaternion starting position in the vector 
    %   6 variables per frame = (vxi, vyi, vzi, X, Y, Z)
    l = (i - 1) * 6;
    % Vector part of the quaternion
    vxi = x(l+1);
    vyi = x(l+2);
    vzi = x(l+3);
    % Compute scalar part of the quaternion
    wi = sqrt ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi );

    % Convert Quaternion to Rotation Matrix
    R = quat2rotmatMx ( [wi vxi vyi vzi] );

    % Convert Rotation Matrix to Roll, Pitch and Yaw
    [Roll Pitch Yaw] = RotMat2MobileRPY ( R );

    % Compute Residuals
    r(k  ) = CamOriData(2, j) - Roll;
    r(k+1) = CamOriData(3, j) - Pitch;
    r(k+2) = CamOriData(4, j) - Yaw;

    % Initially the difference cannot be smaller than -2*pi or bigger than 2*pi
    % sinse the source angles are in the range [-pi, pi]
    % So, only take care that the result is in the range [-pi, pi]
    for l = 0 : 2;
      if r(k+l) > pi;  r(k+l) = r(k+l) - 2.0 * pi; end;
      if r(k+l) < -pi; r(k+l) = r(k+l) + 2.0 * pi; end;
    end 

    % Next Angle Reading triplet in the residual vector
    k = k + 3;
  end
end
