%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for Reference (Fiducial) Point Readings.
%
%  File          : UVL_OPT_residualsRefPoint.m
%  Date          : 03/04/2006 - 20/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_residualsRefPoint Using Planar Homographies from 2D Mosaic Frame to the
%                            2D Image Frame, the function computes the residual by
%                            warping the (X, Y) Reading to the 2D Image Frame
%                            coordiantes and comparing it with the imaged point.
%
%     Input Parameters:
%      iHw: 3x3xn set of 2D Planar Homographies that transform points in the 2D
%           Mosaic Frame to the 2D Image Frame.
%      RefPointData: 5xk matrix that contain in each row a Fiducial Point Reading.
%                    Each row contains the Image Index; x, y Image Coordinate and
%                    X, Y coordinates in the 2D Mosaic Frame.
%
%     Output Parameters:
%      r: k*2 vector containing the residuals. It contains the x, y
%         residual for each Reference Point Reading.
%

function r = UVL_OPT_residualsRefPoint ( iHw, RefPointData )

  % Residual Vector: Two Elements for each Reference Point Reading
  r = zeros ( size ( RefPointData, 2 ) * 2, 1 );

  % Pointer to the current residual Reference Point Reading
  k = 1;

  % Scan Reference Point Readings
  for j = 1 : size ( RefPointData, 2 )
    i  = RefPointData ( 1, j ); % Get the Image Index
    px = RefPointData ( 2, j ); % Point x coordinate in image
    py = RefPointData ( 3, j ); % Point y coordinate in image
    mx = RefPointData ( 4, j ); % 3D X
    my = RefPointData ( 5, j ); % 3D Y

    % Calculate the Residuals
    P = iHw(:,:,i) * [mx; my; 1];
    r(k  ) = px - P(1) / P(3);
    r(k+1) = py - P(2) / P(3);

    % Next Reference Point Reading pair in the residual vector
    k = k + 2;
  end
end
