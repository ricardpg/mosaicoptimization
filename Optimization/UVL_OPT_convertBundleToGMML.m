%
%  Autor(s)      : Jordi Ferrer Plana & Olivier Delaunoy
%  e-mail        : jferrerp@eia.udg.edu, delaunoy@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute vehicle UTM pose from vehicle wrt. 3D Mosaic Frame.
%
%  File          : UVL_OPT_convertBundleToGMML.m
%  Date          : 03/04/2006 - 19/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%
%  mosaicCalcPoseFromMosaic Compute the Vehicle Pose in UTM world coordinates
%                           according to the input parameter RTs that is the
%                           Camera Pose (rotation and translation) with respecte
%                           to the 3D Mosaic Frame (please see WARNING Notes).
%
%    - WARNING: The pose information that is read in the file (x, y, z,
%               Roll, Pitch, Yaw) is interpreted as:
%               - The (x, y, z) coordinate vector defines the UTM position
%                 of the vehicle in meters.
%               - The (Roll, Pitch, Yaw) vector defines the vehicle angles:
%                   Roll : Angle acording to the x axis of the robot {V}.
%                   Pitch: Angle acording to the y axis of the robot {V}.
%                   Yaw  : Angle acording to the z World axis {W} that 
%                          corresponds to the North. It's a clockwise angle
%                          (it is sign inverted).
%               - By default (when V_Pose_C parameter is not given) the
%                 vehicle and the camera center have the same 3D position and 
%                 a fixed 90� angle between the camera an the vehicle is applied.
%              x
%    {i} u      ^ 
%       +---->--|-------+    - (u, v) The hor. and vert. axis of the 2D Image Plane {i}.
%     v |       |       |    - (x, y) The hor. and vert. axis of the 3D Vehicle Frame {V}.
%       |       | {V}   | y  - The z axis of the vehicle is pointing down.
%       v       +-------->   - The 3D Camera Frame {C} is rotated 90� w.r.t.
%       |       | {C}   | x'   3D Vehicle Frame {V} arround the z.
%       |       |       |
%       +-------|-------+
%               v
%              y'
%
%    - Nomenclature for the transformations used in the code:
%       * 3D Rotation: D_R_S (S = 3D Source Frame; D = 3D Destination Frame)
%        Read as: D_R_S is the rotation that transforms coordinates
%                       in the S Frame to the D Frame.
%       * 3D Translation: D_T_S (S = 3D Source Frame; D = 3D Destination Frame)
%        Read as: D_T_S is the translation of the S frame from the (0, 0, 0)
%                       defined in coordinates of the D frame.
%
%        Example: Transformation of a 3D Point in World Frame {W}
%                 into a 3D point in the Camera Frame {C}:
%                   C_X = C_R_W * W_X + C_T_W
%
%       * In 2D is done in the same way but in non capital letters:
%           d_H_s is a planar transformation from the 2D Source Frame s
%                 to the 2D Destination Frame d.
%
%  Usage:
%
%      M = mosaicCalcPoseFromMosaic ( M, RTs );
%
%     Input Parameters:
%      M: GMML structure representing the mosaic. Only the mosaic origin and
%         the number of nodes will be used.
%      RTs: Vector that, for each node in the mosaic, contains an structure with
%           the C_R_W (cRw) rotation matrix and the translation vector W_T_C
%           (wTc). Although a "w" used in the input RTs, it refers to the
%           3D Mosaic frame.
%
%     Output Parameters:
%      M: The following fields of the input GMML structure representing the
%         mosaic will be calculated:
%          - Vehicle Pose Matrix.
%          - The covariance matrices corresponding to the Homography
%            coefficients is set to 0.
%          - The Mosaic Origin in pixels in respect to the UTM coordinates.
%          - The Mosaic Size in pixels.
%

function M = UVL_OPT_convertBundleToGMML ( M, wHi, x )
  % Test the Input Parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check Number of Nodes
  NumNodes = numel ( M.Nodes );
  if NumNodes ~= size ( wHi, 3 );
    error ( 'MATLAB:mosaicCalcPoseFromMosaic:Input', ...
            'Number of nodes in M and number of homographies must agree!' );
  end
  
% % Setting the altitude to 1 meter. (result of optim in meters)
% MOpt.init.mosaic_origin.z = (K(1,1) + K(2,2)) / 2;
M.Header.MosaicResolution = 1;


  
  
  
  
  
  

  % Compute the Transformation 3D Mosaic Frame -> 3D World Frame: W_X = W_R_M * M_X + W_T_M
  % Mosaic Rotation w.r.t. 3D World Frame
  W_R_M = RotX ( pi );
  % Mosaic Translation w.r.t. 3D World Frame
  % (The mosaic is translated according to the X, Y origin but not in moved in Z).
  W_T_M = [ M.Header.MosaicOrigin.X; M.Header.MosaicOrigin.Y; 0 ];

  % Compute the Transformation 3D Camera Frame -> 3D Vehicle Frame: V_X = V_R_C * C_X + V_T_C
  % Camera Rotation w.r.t. 3D Vehicle Frame
  V_R_C = RotZ ( pi/2 );
  C_R_V = V_R_C';

  IndexPoseVar = 1;
  % Scan Camera Poses
  for i = 1 : NumNodes;
    H = wHi(:,:,i);
    M.Nodes(i).Homo.Matrix = H / H(3,3);
    M.Nodes(i).Homo.Model = 'pro';

    % Vector part of the Quaternion
    vxi = x(IndexPoseVar);
    vyi = x(IndexPoseVar+1);
    vzi = x(IndexPoseVar+2);
    % Scalar part of the Quaternion
    wi = sqrt ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi );
     
    % Camera Pose with respect to 3D Mosaic Frame.
    %   RTs: cRw  +  wTc  =>  "w" == "3D Mosaic Frame"
    C_R_M = quat2rotmatMx ( [wi vxi vyi vzi] );        % Convert Quaternion to Rotation Matrix
    M_R_C = C_R_M';
    M_T_C = x(IndexPoseVar+3:IndexPoseVar+5);

    
    % Compute rotation and translation from 3D Vehicle Frame to 3D Mosaic Frame.
    M_R_V = M_R_C * C_R_V;
    M_T_V = M_T_C;

    % Compute rotation and translation from 3D Vehicle Frame to 3D World Frame.
    W_R_V = W_R_M * M_R_V;
    W_T_V = W_R_M * M_T_V + W_T_M;

    % Vehicle Rotation w.r.t. 3D World Frame (Computed allways in mobile axis: post-multiplication)
    %   Rotation in X (pi): Point the Z axis down (Vehicle frame is like that).
    %                       As yaw is a clockwise angle, now is rotation in the right direction.
    %   Rotation in Z (Yaw-pi/2): First rotation with respect to the heading angle. 
    %                             -pi/2 because the heading is according to the north.
    %   Rotation in Y (Pitch): Rotation of the Vehicle in Y direction according to the gravity vector.
    %   Rotation in X (Roll): Rotation of the Vehicle in X direction according to the gravity vector.
    %     W_R_C = RotX ( pi ) * RotZ ( Yaw - pi/2 ) * RotY ( Pitch ) * RotX ( Roll );
    % Inverse Approach to go back to Roll Pitch and Heading angles:    
    % Elimiate RotX:
    W_R_V = RotX ( pi )' * W_R_V;
    [Roll Pitch Yawi] = RotMat2FixedXYZ ( W_R_V );
    Yaw = Yawi + pi/2;
    
    M.Nodes(i).Pose = [ W_T_V' 0 Roll Pitch Yaw ];
    IndexPoseVar = IndexPoseVar + 6;
  end
end
