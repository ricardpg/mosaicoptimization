%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : 3D Vehicle Trajectory Parameters Optimization.
%
%  File          : UVL_OPT_optimize.m
%  Date          : 14/03/2006 - 06/04/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_optimize Perform a Bundle Adjustement of a 3D Vehicle Trajectory
%                   using the Intrinsics (K) extrinsics (HiW), a Point-Match
%                   set of correspondences, Reference Point Readings, Camera
%                   Position Readings and Camera Orientation Readings.
%
%     Input Parameters:
%      K: 3x3xn Intrinsic parameters matrix/matrices. If it is only one is
%         used the same for every frame.
%      RTs: Array for NbrImages nodes containing for each one the
%           structure with the fields:
%        cRw: Rotation of the 3D World Frame w.r.t. the 3D Camera Frame.
%        wTc: Translation of the 3D Camera Frame w.r.t. the 3D World Frame.
%      NumIter: Maximum number of iterations. If it is zero, the user will be
%               asked each time until zero is entered.
%      WPointMatches: Scalar containing the weight for the Point-Match residuals.
%      PointMatchesIdx: See UVL_OPT_residualsPointMatches.m.
%      PointMatchesData: See UVL_OPT_residualsPointMatches.m.
%      WRefPoint: Scalar containing the weight for the Reference (Fiducial) Point
%                 residuals.
%      RefPointData: See UVL_OPT_residualsRefPoint.m.
%      WCamPos: Weight for the Camera Position reading residuals. It can be scalar
%               or a vector with different weights. If it is a vector the size
%               must fit the size of CamPosData.
%      CamPosData: See UVL_OPT_residualsCamPos.m.
%      WCamOri: 3x1 vector containing the independent weights for the
%               Camera Orientation readings residuals or an empty vector.
%      CamOriData: See UVL_OPT_residualsCamOri.m.
%      WMosaicMatches: Scalar containing the weight for the Point-Matches
%                      between different mosaics.
%      MosaicMatches: 6xn vector containing the frame index, frame index',
%                     x, y, x', y' beeing point correspondences between
%                     different mosaics.
%      DoNormalization: If it is true, perform variable normalization. By
%                       default is false.
%      PlotDebug: Plot the residual figures at each step.
%
%     Output Parameters:
%      iHw0: 3x3xNbrImages set of initial Planar Homographies that
%            convert coordinates from 2D World Frame to 2D Image Frame.
%      wHi0: 3x3xNbrImages set of initial Planar Homographies that
%            convert coordinates from 2D Image Frame to 2D World Frame.
%      iHw: 3x3xNbrImages set of Optimized Planar Homographies that
%           convert coordinates from 2D World Frame to 2D Image Frame.
%      wHi: 3x3xNbrImages set of Optimized Planar Homographies that
%           convert coordinates from 2D Image Frame to 2D World Frame.
%      x: 6*NbrImages vector containing the poses of the vehicle in format
%         (vxi, vyi, vzi, X, Y, Z).
%      xt: 6*NbrImagesxt matrix containing the t steps history evolution of
%          the poses in x.
%      Jacobian: mxn matrix containing the evaluation of the Jacobian in
%                the last iteration.
%      JPattern: mxn matrix containing the Pattern to use the Jacobian
%                Matrix. To know m and n see the ComputeJacobianPattern
%                function.
%      Residual: vector containing the residuals for the last step.
%      Residualt: Matrix containing the t steps history evolution of the
%                 residuals.
%      Normalization: In case that DoNormalization parameter is true, this
%                     vector contains Cx, Cy, Cz translation and S scale
%                     used to normalize the data. In case that
%                     DoNormalization is false this vector is empty.
%

function [iHw0 wHi0 iHw wHi x xt Jacobian JPattern Residual Residualt Normalization] = ...
     UVL_OPT_optimize ( K, RTs, NumIter, WPointMatches, PointMatchesIdx, PointMatchesData, ...
                        WRefPoint, RefPointData, WCamPos, CamPosData, ...
                        WCamOri, CamOriData, WMosaicMatches, MosaicMatches, ...
                        DoNormalization, PlotDebug )
  % Test the input parameters
  error ( nargchk ( 15, 16, nargin ) );
  error ( nargoutchk ( 11, 11, nargout ) );

  % Optional parameters
  if nargin < 14; PlotDebug = false; end;
  
  % Check Weights
  if isempty ( WPointMatches ) || ~isscalar ( WPointMatches );
      error ( 'MATLAB:UVL_OPT_optimize:Input', 'WPointMatches must be an scalar!' );
  end

  if isempty ( WRefPoint ) || ~isscalar ( WRefPoint );
      error ( 'MATLAB:UVL_OPT_optimize:Input', 'WRefPoint must be an scalar!' );
  end

  if isempty ( WCamPos ) || ( ~isscalar ( WCamPos ) && ( size ( WCamPos, 1 ) / 3 ) ~= size ( CamPosData, 2 ) );
      error ( 'MATLAB:UVL_OPT_optimize:Input', 'WCamPos must be an scalar or a vector with as many elements as columns * 3 in CamPosData!' );
  end

  if isempty ( WCamPos ) || ( ~isscalar ( WCamPos ) && size ( WCamOri, 1 ) ~= 3 && size ( WCamOri, 1 ) / 3 ~= size ( CamOriData, 2 ) );
    error ( 'MATLAB:UVL_OPT_optimize:Input', 'WCamOri must be a 3x1 vector or a vector with as many elements as columns * 3 in CamOriData!' );
  end
  
  if isempty ( WMosaicMatches ) || ~isscalar ( WMosaicMatches );
      error ( 'MATLAB:UVL_OPT_optimize:Input', 'WMosaicMatches must be an scalar!' );
  end

  % Calculate whether the residuals for each set must be calculated or not.
  if WPointMatches == 0; PointMatchesData = []; PointMatchesIdx = []; end;
  if WRefPoint == 0; RefPointData = []; end;
  if WCamPos == 0; CamPosData = []; WCamPos = 0; end; % set WCamPos to scalar
  if WCamOri == 0; CamOriData = []; WCamOri = 0; end; % set WCamOri to scalar
  CompPointMatches = ~isempty ( PointMatchesData ) && ~isempty ( PointMatchesIdx );
  CompRefPoint = ~isempty ( RefPointData );
  CompCamPos = ~isempty ( CamPosData );
  CompCamOri = ~isempty ( CamOriData );
  CompMosaicMatches = ~isempty ( MosaicMatches );
  
  if PlotDebug;
    % Compute How many residuals there are in each set
    [ NumPointMatches NumRefPoint NumCamPos NumCamOri NumMosaicMatches ] = ...
             UVL_OPT_calcResOffsets ( size ( PointMatchesData ), size ( PointMatchesIdx ), ...
                                      size ( RefPointData ), size ( CamPosData ), ...
                                      size ( CamOriData ), size ( MosaicMatches ) );
    % Create figures to plot residuals
    [ ResPointMatchesFig ResRefPointFig ResCamPosFig ResCamOriFig ResMosaicMatchesFig ] = ...
       UVL_OPT_createResidualFigures ( NumPointMatches, NumRefPoint, NumCamPos, NumCamOri, NumMosaicMatches );
  end
      
  % Number of Variables: vxi, vyi, vzi, x, y, z
  NumVariables = 6;

  % Use initial values if provided, otherwise compute the initial set
  disp ( 'Calculating Initial Variables...' );
  N = numel ( RTs );
  x = zeros ( N * NumVariables, 1 );
  o = 1 : NumVariables;
  for i = 1 : N;
    % Compute Quaternion from Rotation Matrix
    q = rotmat2quatMx ( RTs(i).cRw );
    % Use the vector part to represent the rotation and the translation
    x(o) = [ q(2:4); RTs(i).wTc ];

    o = o + NumVariables;
  end

  % Multiple Shape Parameters
  if size ( K, 1 ) == 3 && size ( K, 2 ) == 3 && size ( K, 3 ) == 1;
    K = repmat ( K, [ 1 1 N ] );
  elseif size ( K, 1 ) == 3 && size ( K, 2 ) == 3 && size ( K, 3 ) == N;
    % Nothing TO DO
  else
    error ( 'MATLAB:UVL_OPT_optimize:Input', 'Variable K (%d,%d,%d) must be 3x3 or 3x3x%d!', ...
            size ( K, 1 ), size ( K, 2 ), size ( K, 3 ), N );
  end

  % Calculate the Number Of Images
  TotalVars = length ( x );
  NbrImages = TotalVars / NumVariables;

  % Test the sizes
  if NbrImages ~= size ( RTs, 2 );
    error ( 'MATLAB:UVL_OPT_optimize:Input', 'Variable vector size does not match!' );
  end

  % Calculate Initial Set of Homographies
  disp ( 'Calculating Initial Homographies...' );
  [iHw0 wHi0] = UVL_OPT_calculateHomographies ( x, K );

  % Normalize Data
  if DoNormalization;
    disp ( 'Normalizing Data...' );
    % Normalize X, Y, Z
    Idx = 1:NumVariables:TotalVars;
    X = x(Idx+3);
    Y = x(Idx+4);
    Z = x(Idx+5);

    MinX = min ( X ); MaxX = max ( X );
    MinY = min ( Y ); MaxY = max ( Y );
    MinZ = min ( Z ); MaxZ = max ( Z );

    Sx = (MaxX - MinX) / 2;
    Sy = (MaxY - MinY) / 2;
    Sz = (MaxZ - MinZ) / 2;

    S = max ( [Sx Sy Sz] );

    Cx = (MinX + MaxX) / 2;
    Cy = (MinY + MaxY) / 2;
%    Cz = (MinZ + MaxZ) / 2;
    Cz = 0;

    if S < eps, S = 1; end
    x(Idx+3) = (X - Cx) / S;
    x(Idx+4) = (Y - Cy) / S;
    x(Idx+5) = (Z - Cz) / S;

    if ~isempty ( CamPosData );
      % Camera Position Readings are in the same frame than Camera Poses:
      %   Use the same parameters to normalize them.
      CamPosData(2,:) = ( CamPosData(2,:) - Cx ) / S;
      CamPosData(3,:) = ( CamPosData(3,:) - Cy ) / S;
      CamPosData(4,:) = ( CamPosData(4,:) - Cz ) / S;
    end

    if ~isempty ( RefPointData );
      % Reference (Fiducial) Point Readings are in the same frame than Camera Poses:
      %   Use the same parameters to normalize them.
      RefPointData(4,:) = ( RefPointData(4,:) - Cx ) / S;
      RefPointData(5,:) = ( RefPointData(5,:) - Cy ) / S;
    end
    Normalization = [Cx Cy Cz S];
    WCamPosS = WCamPos * S;
    WRefPointS = WRefPoint * S;
  else
    Normalization = [];
    WCamPosS = WCamPos;
    WRefPointS = WRefPoint;
  end

  % Initialize the triplet for the sparse matrix (Number of Jacobian entries in the Jacobian Sparse Matrix)
  NumJac = size ( PointMatchesData, 2 ) * size ( PointMatchesData, 1 ) * 2 * NumVariables + ...
           size ( RefPointData, 2 )                                    * 2 * NumVariables + ...
           size ( CamPosData, 2 )                                      * 1 * 3 + ...
           size ( CamOriData, 2 )                                      * 3 * 3 + ...
           size ( MosaicMatches, 2 ) * 4 * 2 * NumVariables;

  % Setup the Triplet of Row, Col, Value
  Triplet.Col = zeros ( NumJac, 1 );
  Triplet.Row = zeros ( NumJac, 1 );
  Triplet.Val = zeros ( NumJac, 1 );

  % Number of Residuals
  NumRes = size ( PointMatchesData, 1 ) * size ( PointMatchesData, 2 ) + ...
           2                            * size ( RefPointData, 2 ) + ...
           3                            * size ( CamPosData, 2 ) + ...
           3                            * size ( CamOriData, 2 ) + ...
           4                            * size ( MosaicMatches, 2 );

  % Compute the indices for each different vector part of the quaternion
  QX = 1 : NumVariables : numel(x);
  QY = QX + 1;
  QZ = QY + 1;

  % Compute the Jacobian checking function if they are not provided
  disp ( 'Calculating Jacobian Pattern...' );
  JPattern = ComputeJacobianPattern;

  % Residual History
  lt = 0;
  rt = 0;

  Options = optimset ( 'lsqnonlin' ); % Default
  Options.Display = 'iter';
  Options.MaxFunEvals = [];
  Options.TolFun = 10^-15;
  Options.TolX = 10^-15;
  Options.FunValCheck = 'on';
  Options.OutputFcn = @OutFunction;
  Options.Jacobian = 'on';
  Options.JacobPattern = JPattern;
  Options.PrecondBandWidth = inf;
%  Options.MaxPCGIter = 100;
  Options.TypicalX = [];
%  Options.DerivativeCheck = 'on';
%  Options.DiffMaxChange = [];
%  Options.DiffMinChange = [];
  Options.UseParallel = 'always';
  Options.ScaleProblem = true;

  % Continue iterations
  if isempty ( NumIter ) || ~isscalar ( NumIter ) || NumIter == 0;
    NumIter = 0;
    while NumIter <= 0;
      NumIt = input ( 'Enter how many initial iterations: ' );
      NumIter = fix ( NumIt );
    end

    AskIterations = true;
  else
    AskIterations = false;
  end

  while NumIter > 0;
    Options.MaxIter = NumIter;

    disp ( 'Starting Iterations...' );
    % Run the Optimization
    if PlotDebug; tic; end;

    [x, ResNorm, Residual, ExitFlag, Output, Lambda, Jacobian] = lsqnonlin ( @costFun, x, [], [], Options );
    if AskIterations;
      NumIt = input ( 'Enter how many iterations more: ' );
      NumIter = fix ( NumIt );
    else
      NumIter = 0;
    end
  end

  % Unnormalize data
  if DoNormalization;
    disp ( 'Unnormalizing Data...' );
    X = x(Idx+3);
    Y = x(Idx+4);
    Z = x(Idx+5);

    x(Idx+3) = X * S + Cx;
    x(Idx+4) = Y * S + Cy;
    x(Idx+5) = Z * S + Cz;
  end

  % Calculate Final Set of Optimized Homographies
  [iHw wHi] = UVL_OPT_calculateHomographies ( x, K );



  % Function to store history and show plots and time of intermediate values 
  %
  function Stop = OutFunction ( xi, OptimValues, State )
    switch ( State );
      case 'init'
        fprintf ( '\nIterations started at "%s"\n\n', datestr(now) );
      case 'interrupt'
      case 'done'
      case 'iter'
        if PlotDebug;
          % Plot residual
          UVL_OPT_plotResidual ( OptimValues.residual, WPointMatches, WRefPoint, WCamPos, WCamOri, WMosaicMatches, ...
                                 ResPointMatchesFig, ResRefPointFig, ResCamPosFig, ResCamOriFig, ResMosaicMatchesFig, ...
                                 NumPointMatches, NumRefPoint, NumCamPos, NumCamOri, NumMosaicMatches );
          drawnow;

          fprintf ( '\b   (%s) %04d:%05.3f\n', datestr(now), fix(toc / 60), mod(toc,60) );
          tic;
        end

        % Store x history
        if DoNormalization;
          Idx = 1:NumVariables:TotalVars;

          X = xi(Idx+3);
          Y = xi(Idx+4);
          Z = xi(Idx+5);

          xi(Idx+3) = X * S + Cx;
          xi(Idx+4) = Y * S + Cy;
          xi(Idx+5) = Z * S + Cz;
        end

        lt = lt + 1;
        xt(lt, :) = xi;

        % Store Residual history
        rt = rt + 1;  
        Residualt(:, rt) = OptimValues.residual;
      otherwise
        disp ( 'Unknown State in OutFunction()' );
    end

    Stop = false;
  end


  % Cost function to compute Jacobian and Residuals
  %
  function [r J] = costFun ( xi )
      Vxi = xi(QX);
      Vyi = xi(QY);
      Vzi = xi(QZ);

      % Check whether scalar part of quaternion can be computed or not (wi = sqrt ( 1.0 - Vxi^2 - Vyi^2 - Vzi^2 ) )
      Sq = -(Vxi .* Vxi) - (Vyi .* Vyi) - (Vzi .* Vzi);

      Idx = find ( Sq < -1.0 );
      if ~isempty ( Idx );
        qd = zeros ( size ( Idx, 1 ), 4 );
   
        % Quaternion
        % [ cosA/2, sinA/2*vx, sinA/2*vy, sinA/2*vz ]
        % [ wi, vxi, vyi, vzi ]
        %
        %  norm ( [vxi, vyi, vzi] ) = sinA/2
        %  sqrt ( vxi^2 + vyi^2 + vzi^2 ) = sinA/2
        %
        %  A/2 = asin ( sqrt ( vxi^2 + vyi^2 + vzi^2 ) )
        qd(:,2) = Vxi(Idx);
        qd(:,3) = Vyi(Idx);
        qd(:,4) = Vzi(Idx);
        qd(:,1) = cos ( asin ( sqrt ( qd(:,2) .^ 2 + qd(:,3) .^ 2 + qd(:,4) .^2 ) - 1.0 ) );

%        q1 = zeros ( size ( Idx, 1 ), 4 );
%        q2 = zeros ( size ( Idx, 1 ), 4 );
%         % sqrt ( -wi ) = [ 0, sqrt(wi)*i, 0*j, 0*k ]
% 
% % function qd = quatprod ( q1, q2 )
% %   q01 = q1(1); qx1 = q1(2); qy1 = q1(3); qz1 = q1(4);
% %   q02 = q2(1); qx2 = q2(2); qy2 = q2(3); qz2 = q2(4);
% % 
% %   qd(1) = q01 * q02 - qx1 * qx2 - qy1 * qy2 - qz1 * qz2;
% %   qd(2) = q01 * qx2 + qx1 * q02 + qy1 * qz2 - qz1 * qy2;
% %   qd(3) = q01 * qy2 - qx1 * qz2 + qy1 * q02 + qz1 * qx2;
% %   qd(4) = q01 * qz2 + qx1 * qy2 - qy1 * qx2 + qz1 * q02;
% % end
% 
%         q1(:,1) = 0;
%         q1(:,2) = sqrt ( -Sq(Idx) - 1.0 );
%         q1(:,3) = 0;
%         q1(:,4) = 0;
% 
%         q2(:,1) = 0;
%         q2(:,2) = xi(QXIdx);
%         q2(:,3) = xi(QYIdx);
%         q2(:,4) = xi(QZIdx);
% 
% % function qd = quatprod ( q1, q2 )
% %   qx1 = q1(2);
% %   q02 = q2(1); qx2 = q2(2); qy2 = q2(3); qz2 = q2(4);
% % end
%         
%         % Rotation q1 by q2, yaking into account the 0's in the quaternions
%         %   qd(1) = - qx1 * qx2;
%         %   qd(2) = 0;
%         %   qd(3) = - qx1 * qz2;
%         %   qd(4) = + qx1 * qy2;
%         qd(:,1) = -q1(:,2) .* q2(:,2);
%         qd(:,2) = 0;
%         qd(:,3) = -q1(:,2) .* q2(:,4);
%         qd(:,4) =  q1(:,2) .* q2(:,3);

        Norms = sqrt ( qd(:,1) .^ 2.0 + qd(:,2) .^ 2.0 + qd(:,3) .^ 2.0 + qd(:,4) .^ 2.0 );

        fprintf ( 2, '  WARNING!!! Renormalizing %d/%d quaternions (bad initial estimation or weights?)!\n', size ( Idx, 1 ), size ( Sq, 1 ) );

        % Store Normalized value
        xi(QX(Idx)) = qd(:,2) ./ Norms;
        xi(QY(Idx)) = qd(:,3) ./ Norms;
        xi(QZ(Idx)) = qd(:,4) ./ Norms;
      end

      % Calculate Homographies iHw and wHi from the pose parameters
      if CompPointMatches || CompRefPoint;
        [iHw wHi] = UVL_OPT_calculateHomographies ( xi, K );
      end

      % Compute residuals from point and matches
      if CompPointMatches;
        ResPointMatches = UVL_OPT_residualsPointMatches ( iHw, wHi, PointMatchesData, PointMatchesIdx ) .* WPointMatches;
      else
        ResPointMatches = [];
      end

      % Compute residuals from known Reference (Fiducial) Points
      if CompRefPoint;
        ResRefPoint = UVL_OPT_residualsRefPoint ( iHw, RefPointData ) .* WRefPointS;
      else
        ResRefPoint = [];
      end

      % Compute residuals from known Camera Position Readings
      if CompCamPos;
        ResCamPos = UVL_OPT_residualsCamPos ( xi, CamPosData ) .* WCamPosS;
      else
        ResCamPos = [];
      end

      % Compute residuals from known Camera Orientation Readings
      if CompCamOri;
        ResCamOri = UVL_OPT_residualsCamOri ( xi, CamOriData ) .* WCamOri;
      else
        ResCamOri = [];
      end
      
      % Compute residuals from known Matches between Mosaics
      if CompMosaicMatches;
        ResMosaicMatches = UVL_OPT_residualsMosaicMatches ( iHw, wHi, MosaicMatches ) .* WMosaicMatches;
      else
        ResMosaicMatches = [];
      end

      % Merge residuals 
      r = [ ResPointMatches; ResRefPoint; ResCamPos; ResCamOri; ResMosaicMatches ];

      % When 2 output arguments: Calculate Jacobian
      if nargout > 1;
          % Index in the triplet
          t = 1;

          % Jacobian entries from Point Matches
          NumVars = NumVariables * 4 * 2;
          row = 1:NumVariables;
          if ~isempty ( PointMatchesData ) && ~isempty ( PointMatchesIdx );
            for n = 1 : size ( PointMatchesData, 2 );
              % Compute inidices to the variables of the Current and the Reference Image
              i = PointMatchesIdx ( 1, n );
              j = PointMatchesIdx ( 2, n );
              Ii = (i - 1) * NumVariables;
              Ij = (j - 1) * NumVariables;
              rows = [ row+Ii row+Ij ];

              % Obtain Ks
              Ki = K(:,:,i);
              Kj = K(:,:,j);

              Vars = [ Ki([1 5 7 8])'; Kj([1 5 7 8])'; xi(rows) ];
              for m = 1 : 4 : size ( PointMatchesData, 1 );
                  Points = PointMatchesData(m:m+3, n);
                  [J1, J2, J3, J4] = UVL_OPT_jacobianPointMatchesMx ( [Vars; Points(:)] );
                  Triplet.Val(t:t+NumVars-1) = WPointMatches .* [J1; J2; J3; J4];
                  t = t + NumVars;
              end
            end
          end

          % Jacobian entries from Reference (Fiducial) Points
          if ~isempty ( RefPointData )
              for n = 1 : NbrImages;
                  % Find the Reference Points appeared in image i
                  Idx = RefPointData(1, :) == n;
                  if any ( Idx );
                      Points = RefPointData(4:5, Idx);
                      % Compute inidices to the variables of the Current Image
                      Iil = ( n - 1 ) * NumVariables + 1;
                      Iir = Iil + NumVariables - 1;
                      rows = Iil:Iir;

                      Kn = K(:,:,n);

                      Vars = [ Kn([1 5 7 8])'; xi(rows) ];
                      for m = 1 : size(Points,2);
                          [J1, J2] = UVL_OPT_jacobianRefPointMx ( [Vars; Points(:,m)] );
                          Triplet.Val(t:t+NumVariables*2-1) = WRefPointS .* [J1; J2];
                          t = t + NumVariables * 2;
                      end

                  end
              end
          end

          % Jacobian entries from Camera Position Readings
          if ~isempty ( CamPosData )
              Triplet.Val(t : t + size ( CamPosData, 2 ) * 3 - 1) = -WCamPosS;
              t = t + size ( CamPosData, 2 ) * 3;
          end
          
          % Jacobian entries from Angular Camera Readings
          l = 1;
          row = 1:3;
          if ~isempty ( CamOriData ),
            Sequence = sort(CamOriData(1, :));
            for n = Sequence,
                % Compute inidices to the variables for the Current Image 
                % (Only the quaternion is needed)
                Ii = ( n - 1 ) * NumVariables;
                rows = row + Ii;

                [J1, J2, J3] = UVL_OPT_jacobianCamOriMx ( xi(rows) );
                if isscalar ( WCamOri ),
                    Triplet.Val(t:t+8) = WCamOri .* [J1; J2; J3];
                else
                    Triplet.Val(t:t+2) = WCamOri(l) .* J1;
                    Triplet.Val(t+3:t+5) = WCamOri(l+1) .* J2;
                    Triplet.Val(t+6:t+8) = WCamOri(l+2) .* J3;
                    l = l + 3;
                end
                t = t + 9;
            end
          end
          
          % Jacobian entries from Mosaic Matches
          NumVars = NumVariables * 4 * 2;
          row = 1:NumVariables;
          if ~isempty ( MosaicMatches );
            for n = 1 : size ( MosaicMatches, 2 );
              % Compute inidices to the variables of the Current and the Reference Image
              i = MosaicMatches ( 1, n );
              j = MosaicMatches ( 2, n );
              Ii = (i - 1) * NumVariables;
              Ij = (j - 1) * NumVariables;
              rows = [ row+Ii row+Ij ];

              % Obtain Ks
              Ki = K(:,:,i);
              Kj = K(:,:,j);

              Vars = [ Ki([1 5 7 8])'; Kj([1 5 7 8])'; xi(rows) ];
              Points = MosaicMatches ( 3:6, n );
              [J1, J2, J3, J4] = UVL_OPT_jacobianPointMatchesMx ( [Vars; Points(:)] );
              Triplet.Val(t:t+NumVars-1) = WMosaicMatches .* [J1; J2; J3; J4];
              t = t + NumVars;
            end
          end

          % should switch the rows and the cols but faster like that ...
          J = sparse ( Triplet.Col, Triplet.Row, Triplet.Val, TotalVars, NumRes );
          J = J';  
      end
  end



  % Compute the Sparse Matrix to represent whether a Jacobian must be computed or not
  %
  % IMPORTANT NOTE: The RefPointData, CamPosData and CamOriData matrices MUST
  %                 be sorted by the image index (First column).
  %
  function J = ComputeJacobianPattern
      Triplet.Row(:) = 0;
      Triplet.Col(:) = 0;
      Triplet.Val(:) = 1;

      % Index to the Row
      k = 1;
      % Index in the triplet
      t = 1;

      NumVars = NumVariables * 2;
      NumCorr = size ( PointMatchesData, 1 );
      row = (1:NumVariables)';
      if ~isempty ( PointMatchesData ) && ~isempty ( PointMatchesIdx );
        for n = 1 : size ( PointMatchesData, 2 );
          % Compute inidices to the variables of the Current and the Reference Image
          Ii = (PointMatchesIdx ( 1, n ) - 1) * NumVariables;
          Ij = (PointMatchesIdx ( 2, n ) - 1) * NumVariables;
          rows = [row+Ii; row+Ij];

          for m = 1 : NumCorr;
              Triplet.Col(t:t+NumVars-1) = rows;
              Triplet.Row(t:t+NumVars-1) = k;
              k = k + 1;
              t = t + NumVars;
          end
        end
      end

      if ~isempty ( RefPointData )
          for n = 1 : NbrImages;
              % Find the Reference (Fiducial) Points appeared in image i
              Idx = RefPointData(1, :) == n;
              if any ( Idx );
                  Points = RefPointData(4:5, Idx);
                  % Compute inidices to the variables of the Current Image
                  Iil = ( n - 1 ) * NumVariables + 1;
                  Iir = Iil + NumVariables - 1;
                  rows = (Iil:Iir)';

                  for m = 1 : size(Points,2);
                      Triplet.Col(t:t+NumVariables-1) = rows;
                      Triplet.Col(t+NumVariables:t+NumVariables*2-1) = rows;
                      Triplet.Row(t:t+NumVariables-1) = k;
                      Triplet.Row(t+NumVariables:t+NumVariables*2-1) = k+1;
                      k = k+2;
                      t = t+NumVariables*2;
                  end
              end
          end
      end

      row = (4:6)';
      if ~isempty ( CamPosData ),
        Sequence = sort ( CamPosData ( 1, : ) );
        for n = Sequence,
            % Compute inidices to the variables of the Current Image
            Ii = ( n - 1 ) * NumVariables;
            rows = row + Ii;

            % Assign Jacobian   % The Jacobian Cam are -1
            Triplet.Col(t:t+2) = rows;
            Triplet.Row(t:t+2) = k:k+2;
            k = k + 3;
            t = t + 3;
        end
      end

      row = (1:3)';
      if ~isempty ( CamOriData ),
        Sequence = sort(CamOriData(1, :));
        for n = Sequence,
            % Compute inidices to the variables for the Current Image 
            % (Only the quaternion is needed)
            Ii = ( n - 1 ) * NumVariables;
            rows = row + Ii;

            Triplet.Col(t:t+2) = rows;
            Triplet.Col(t+3:t+5) = rows;
            Triplet.Col(t+6:t+8) = rows;
            Triplet.Row(t:t+2) = k;
            Triplet.Row(t+3:t+5) = k+1;
            Triplet.Row(t+6:t+8) = k+2;
            k = k + 3;
            t = t + 9;
        end
      end

      NumVars = NumVariables * 2;
      NumCorr = size ( MosaicMatches, 1 ) - 2;
      row = (1:NumVariables)';
      if ~isempty ( MosaicMatches );
        for n = 1 : size ( MosaicMatches, 2 );
          % Compute inidices to the variables of the Current and the Reference Image
          Ii = (MosaicMatches ( 1, n ) - 1) * NumVariables;
          Ij = (MosaicMatches ( 2, n ) - 1) * NumVariables;
          rows = [row+Ii; row+Ij];

          for m = 1 : NumCorr;
            Triplet.Col(t:t+NumVars-1) = rows;
            Triplet.Row(t:t+NumVars-1) = k;
            k = k + 1;
            t = t + NumVars;
          end
        end
      end

      % should switch the rows and the cols but faster like that ...
      J = sparse ( Triplet.Col, Triplet.Row, Triplet.Val, TotalVars, NumRes );
      J = J';
  end
end
