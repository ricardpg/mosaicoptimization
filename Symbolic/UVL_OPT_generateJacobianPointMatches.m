%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobian for Point and Match Algebraically.
%
%  File          : UVL_OPT_generateJacobianPointMatches.m
%  Date          : 26/04/2006 - 06/04/2009
%
%  Compiler      : MATLAB >= 7.7 (R2008b)
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_generateJacobianPointMatches Calculate the Jacobian for the Point-Match data
%                                       algebraically. The function creates a Matlab's
%                                       C++ Mex file.
%
%     Input Parameters:
%      JacobianPointMatchesFileName: String containing the C++ generated file name.
%                                    By default is "UVL_OPT_jacobianPointMatchesMx.cpp".
%
%     Output Parameters:
%

% function UVL_OPT_generateJacobianPointMatches ( JacobianPointMatchesFileName )
%   % Test the input parameters
%   error ( nargchk ( 0, 1, nargin ) );
%   error ( nargoutchk ( 0, 0, nargout ) );
%
%   % Default value for the output filename
%   if nargin < 1 
%     JacobianPointMatchesFileName = 'JacobianPointMatchesMx.cpp';
%   end

  if ~exist ( 'JacobianPointMatchesFileName', 'var' );
    JacobianPointMatchesFileName = 'UVL_OPT_jacobianPointMatchesMx.cpp';
  end
    
  % Flags for simplifying equations
  DoSimplifyw = false;
  DoSimplifyAbsoluteHomograhies = false;
  DoSimplifyRelativeHomograhies = false;
  DoSimplifyCosts = false;
  DoSimplifyJacobian = true;

  % Symbolic Quaternion Fields for Image i and j
  vxi = sym ( 'vxi', 'real' );
  vyi = sym ( 'vyi', 'real' );
  vzi = sym ( 'vzi', 'real' );
  vwi = ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2);
  % Scalar from vector
  if DoSimplifyw;
    wi = vwi;
  else
    wi = sym ( 'wi', 'real' );
  end

  vxj = sym ( 'vxj', 'real' );
  vyj = sym ( 'vyj', 'real' );
  vzj = sym ( 'vzj', 'real' );
  vwj = ( 1.0 - vxj * vxj - vyj * vyj - vzj * vzj )^(1/2);
  % Scalar from vector
  if DoSimplifyw;
    wj = vwj;
  else
    wj = sym ( 'wj', 'real' );
  end

  % Symbolic Translation Parameters for Image i and j
  txi = sym ( 'txi', 'real' );
  tyi = sym ( 'tyi', 'real' );
  tzi = sym ( 'tzi', 'real' );
  txj = sym ( 'txj', 'real' );
  tyj = sym ( 'tyj', 'real' );
  tzj = sym ( 'tzj', 'real' );

  % Symbolic Points and Matches
  px = sym ( 'px', 'real' );
  py = sym ( 'py', 'real' );
  mx = sym ( 'mx', 'real' );
  my = sym ( 'my', 'real' );

  % Symbolic Camera i Parameters
  aui = sym ( 'aui', 'real' );
  avi = sym ( 'avi', 'real' );
  cxi = sym ( 'cxi', 'real' );
  cyi = sym ( 'cyi', 'real' );

  % Symblic Camera Intrinsic Parameters
  Ki = [ aui   0   cxi;
          0   avi  cyi;
          0    0    1 ];
  Ki_1 = inv ( Ki );

  % Symbolic Camera j Parameters
  auj = sym ( 'auj', 'real' );
  avj = sym ( 'avj', 'real' );
  cxj = sym ( 'cxj', 'real' );
  cyj = sym ( 'cyj', 'real' );

  % Symblic Camera Intrinsic Parameters
  Kj = [ auj   0   cxj;
          0   avj  cyj;
          0    0    1 ];
  Kj_1 = inv ( Kj );

  % Symblic Elements of Rotation Matrices Represented by Quaternions for image i and j
  Ri = UVL_OPT_quaternionToRotationMatrix ( [ wi vxi vyi vzi ] );
  Rj = UVL_OPT_quaternionToRotationMatrix ( [ wj vxj vyj vzj ] );

  % Translation for Factorized Projection Matrix 
  Ti = [1 0 -txi; 0 1 -tyi; 0 0 -tzi];
  Tj = [1 0 -txj; 0 1 -tyj; 0 0 -tzj];

  % Compute wHi = K * cRw * wTc
  Hi = Ki * Ri * Ti;
  Hj = Kj * Rj * Tj;

  if DoSimplifyAbsoluteHomograhies;
    disp ( 'Simplifying Absolute Homograhpy Expressions...' );
    Hi = simplify ( Hi );
    Hj = simplify ( Hj );
  end

  % Transpose manually to avoid "conjugate()" appearing
  Rit(1,1) = Ri(1,1); Rit(1,2) = Ri(2,1); Rit(1,3) = Ri(3,1);
  Rit(2,1) = Ri(1,2); Rit(2,2) = Ri(2,2); Rit(2,3) = Ri(3,2);
  Rit(3,1) = Ri(1,3); Rit(3,2) = Ri(2,3); Rit(3,3) = Ri(3,3);

  Rjt(1,1) = Rj(1,1); Rjt(1,2) = Rj(2,1); Rjt(1,3) = Rj(3,1);
  Rjt(2,1) = Rj(1,2); Rjt(2,2) = Rj(2,2); Rjt(2,3) = Rj(3,2);
  Rjt(3,1) = Rj(1,3); Rjt(3,2) = Rj(2,3); Rjt(3,3) = Rj(3,3);

  % Compute iHj and jHi from wHi and wHj
  Hij = Hi * inv(Tj) * Rjt * Kj_1;
  Hji = Hj * inv(Ti) * Rit * Ki_1;

  if DoSimplifyRelativeHomograhies;
    disp (   'Simplifying Relative Homograhy Expressions...' );
    Hij = simplify ( Hij );
    Hji = simplify ( Hji );
  end

  disp ( 'Calculating Residuals for Point and Match (Px, Py, Mx and My)...' );  
  CostPointMatches_Px = px - ((Hij(1,:)*[mx;my;1]) / (Hij(3,:)*[mx;my;1]));
  CostPointMatches_Py = py - ((Hij(2,:)*[mx;my;1]) / (Hij(3,:)*[mx;my;1]));
  CostPointMatches_Mx = mx - ((Hji(1,:)*[px;py;1]) / (Hji(3,:)*[px;py;1]));
  CostPointMatches_My = my - ((Hji(2,:)*[px;py;1]) / (Hji(3,:)*[px;py;1]));

  if DoSimplifyCosts;
    disp ( 'Simplifying Cost Expressions...' );
    disp ( '  Cost for Px...' );
    sCostPointMatches_Px = simplify ( CostPointMatches_Px );
    disp ( '  Cost for Py...' );
    sCostPointMatches_Py = simplify ( CostPointMatches_Py );
    disp ( '  Cost for Mx...' );
    sCostPointMatches_Mx = simplify ( CostPointMatches_Mx );
    disp ( '  Cost for My...' );
    sCostPointMatches_My = simplify ( CostPointMatches_My );
  else
    sCostPointMatches_Px = CostPointMatches_Px;
    sCostPointMatches_Py = CostPointMatches_Py;
    sCostPointMatches_Mx = CostPointMatches_Mx;
    sCostPointMatches_My = CostPointMatches_My;
  end

  % Sizes
  NumJac = 4;
  NumVars = 12;
  JacNames = cell ( NumJac, 1 );
  JacNames{1} = 'Px';
  JacNames{2} = 'Py';
  JacNames{3} = 'Mx';
  JacNames{4} = 'My';

  % Allocate Cell-Array
  dCostPointMatches = cell ( NumJac, 1 );
  dsCostPointMatches = cell ( NumJac, 1 );

  % Replace wi and wj for its value
  if ~DoSimplifyw;
    sCostPointMatches_Px = subs ( sCostPointMatches_Px, wi, vwi );
    sCostPointMatches_Px = subs ( sCostPointMatches_Px, wj, vwj );

    sCostPointMatches_Py = subs ( sCostPointMatches_Py, wi, vwi );
    sCostPointMatches_Py = subs ( sCostPointMatches_Py, wj, vwj );

    sCostPointMatches_Mx = subs ( sCostPointMatches_Mx, wi, vwi );
    sCostPointMatches_Mx = subs ( sCostPointMatches_Mx, wj, vwj );

    sCostPointMatches_My = subs ( sCostPointMatches_My, wi, vwi );
    sCostPointMatches_My = subs ( sCostPointMatches_My, wj, vwj );
  end
  
  % Jacobian for the Point and Match
  disp ( 'Calculating Jacobian for Px...' );
  dCostPointMatches{1} = jacobian ( sCostPointMatches_Px, [vxi, vyi, vzi, txi, tyi, tzi, ...
                                                           vxj, vyj, vzj, txj, tyj, tzj] );

  disp ( 'Calculating Jacobian for Py...' );
  dCostPointMatches{2} = jacobian ( sCostPointMatches_Py, [vxi, vyi, vzi, txi, tyi, tzi, ...
                                                           vxj, vyj, vzj, txj, tyj, tzj] );

  disp ( 'Calculating Jacobian for Mx...' );
  dCostPointMatches{3} = jacobian ( sCostPointMatches_Mx, [vxi, vyi, vzi, txi, tyi, tzi, ...
                                                           vxj, vyj, vzj, txj, tyj, tzj] );

  disp ( 'Calculating Jacobian for My...' );
  dCostPointMatches{4} = jacobian ( sCostPointMatches_My, [vxi, vyi, vzi, txi, tyi, tzi, ...
                                                           vxj, vyj, vzj, txj, tyj, tzj] );

  if DoSimplifyJacobian;
    for j = 1 : NumJac;
      fprintf ( 'Simplifying Jacobian for %s...\n', JacNames{j} );
      for i = 1 : NumVars;
        fprintf ( '  Simplifying Jacobian %d/%d\n', i, NumVars );
        dsCostPointMatches{j}(1,i) = simplify ( dCostPointMatches{j}(1,i) );
      end
    end    
  else
    for j = 1 : NumJac;
      dsCostPointMatches{j} = dCostPointMatches{j};
    end
  end
  
  % Substitutions
  Subs = cell ( 2, 0 );
  i = 1;
  Subs{1,i} = 'vxi*vxi';                    Subs{2,i} = 'vxi2';  i = i + 1;
  Subs{1,i} = 'vyi*vyi';                    Subs{2,i} = 'vyi2';  i = i + 1;
  Subs{1,i} = 'vzi*vzi';                    Subs{2,i} = 'vzi2';  i = i + 1;
  Subs{1,i} = '-vxi2-vyi2-vzi2';            Subs{2,i} = 'wi2';   i = i + 1;
  Subs{1,i} = 'sqrt(wi2+1.0)';              Subs{2,i} = 'wi';    i = i + 1;

  Subs{1,i} = 'vxi*vyi';                    Subs{2,i} = 'vxyi';  i = i + 1;
  Subs{1,i} = 'vxi*vzi';                    Subs{2,i} = 'vxzi';  i = i + 1;
  Subs{1,i} = 'vyi*vzi';                    Subs{2,i} = 'vyzi';  i = i + 1;

  Subs{1,i} = 'vxj*vxj';                    Subs{2,i} = 'vxj2';  i = i + 1;
  Subs{1,i} = 'vyj*vyj';                    Subs{2,i} = 'vyj2';  i = i + 1;
  Subs{1,i} = 'vzj*vzj';                    Subs{2,i} = 'vzj2';  i = i + 1;
  Subs{1,i} = '-vxj2-vyj2-vzj2';            Subs{2,i} = 'wj2';   i = i + 1;
  Subs{1,i} = 'sqrt(wj2+1.0)';              Subs{2,i} = 'wj';    i = i + 1;

  Subs{1,i} = 'vxj*vyj';                    Subs{2,i} = 'vxyj';  i = i + 1;
  Subs{1,i} = 'vxj*vzj';                    Subs{2,i} = 'vxzj';  i = i + 1;
  Subs{1,i} = 'vyj*vzj';                    Subs{2,i} = 'vyzj';  i = i + 1;

  % Create the output Mex file
  FId = fopen ( JacobianPointMatchesFileName, 'w' );
  if FId ~= -1;
    fprintf ( 'Generating "%s" Mex File...\n', JacobianPointMatchesFileName );

    fprintf ( FId, '//\n' );
    fprintf ( FId, '// C++ Automatically generated file using ''%s.m''\n', mfilename );
    fprintf ( FId, '// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>\n' );
    fprintf ( FId, '// Date: %s\n', datestr ( now ) );
    fprintf ( FId, '//\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#include <mex.h>          // Matlab Mex Functions\n' );
    fprintf ( FId, '#include <cmath>\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#define ERROR_HEADER       "MATLAB:%s:Input\\n"\n', JacobianPointMatchesFileName );
    fprintf ( FId, '#define VARIABLES          "Vars = (aui, avi, cxi, cyi, auj, avj, cxj, cyj, vxi, vyi, vzi, txi, tyi, tzi, vxi, vyi, vzi, txj, tyj, tzj, px, py, mx, my)"\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify the input parameters by it''s index\n' );
    fprintf ( FId, 'enum { VARS=0 };\n' );

    fprintf ( FId, '// Identify the output parameters by it''s index\n' );
    fprintf ( FId, 'enum { J1=0, J2, J3, J4 };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify elements in Vars vector by it''s index\n' );
    fprintf ( FId, 'enum { AUI=0, AVI, CXI, CYI, AUJ, AVJ, CXJ, CYJ, VXI, VYI, VZI, TXI, TYI, TZI, VXJ, VYJ, VZJ, TXJ, TYJ, TZJ, PX, PY, MX, MY };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, 'void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )\n' );
    fprintf ( FId, '{\n' );
    fprintf ( FId, '  // Parameter checks\n');
    fprintf ( FId, '  if ( nlhs != 4 || nrhs != 1 )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Usage: [j1, j2, j3, j4] = %s ( Vars )\\n\\n"', JacobianPointMatchesFileName );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  int M = mxGetM ( prhs[VARS] );\n' );
    fprintf ( FId, '  int N = mxGetN ( prhs[VARS] );\n' );
    fprintf ( FId, '  if ( ( M != 1 || N != 24 ) && ( M != 24 || N != 1 ) )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Vars must be a 1x24 or a 24x1 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *Vars = (double *)mxGetPr ( prhs[VARS] );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double aui = Vars[AUI];\n' );
    fprintf ( FId, '  double avi = Vars[AVI];\n' );
    fprintf ( FId, '  double cxi = Vars[CXI];\n' );
    fprintf ( FId, '  double cyi = Vars[CYI];\n' );
    fprintf ( FId, '  double auj = Vars[AUJ];\n' );
    fprintf ( FId, '  double avj = Vars[AVJ];\n' );
    fprintf ( FId, '  double cxj = Vars[CXJ];\n' );
    fprintf ( FId, '  double cyj = Vars[CYJ];\n' );
    fprintf ( FId, '  double vxi = Vars[VXI];\n' );
    fprintf ( FId, '  double vyi = Vars[VYI];\n' );
    fprintf ( FId, '  double vzi = Vars[VZI];\n' );
    fprintf ( FId, '  double txi = Vars[TXI];\n' );
    fprintf ( FId, '  double tyi = Vars[TYI];\n' );
    fprintf ( FId, '  double tzi = Vars[TZI];\n' );
    fprintf ( FId, '  double vxj = Vars[VXJ];\n' );
    fprintf ( FId, '  double vyj = Vars[VYJ];\n' );
    fprintf ( FId, '  double vzj = Vars[VZJ];\n' );
    fprintf ( FId, '  double txj = Vars[TXJ];\n' );
    fprintf ( FId, '  double tyj = Vars[TYJ];\n' );
    fprintf ( FId, '  double tzj = Vars[TZJ];\n' );
    fprintf ( FId, '  double px  = Vars[PX];\n' );
    fprintf ( FId, '  double py  = Vars[PY];\n' );
    fprintf ( FId, '  double mx  = Vars[MX];\n' );
    fprintf ( FId, '  double my  = Vars[MY];\n' );
    fprintf ( FId, '\n' );

    % Perform substitutions
    for i = 1 : size ( Subs, 2 );
      fprintf ( FId, '  double %s = %s;\n', Subs{2,i}, Subs{1,i} );
    end
    fprintf ( FId, '\n' );

    % Check for the negative square root
    fprintf ( FId, '  // Check whether the sqrt() can be computed or not (we have a right vector part of the quaternion!)\n' );
    fprintf ( FId, '  if ( wi2 < -1.0 )\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    mexPrintf ( "Point Matches Jacobian cannot be calculated: (wi2 = %%f) < 0 in sqrt (wi2)!\\n", wi2 );\n' );
    fprintf ( FId, '    wi = 0.0;\n' );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  // Check whether the sqrt() can be computed or not (we have a right vector part of the quaternion!)\n' );
    fprintf ( FId, '  if ( wj2 < -1.0 )\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    mexPrintf ( "Point Matches Jacobian cannot be calculated: (wj2 = %%f) < 0 in sqrt (wj2)!\\n", wj2 );\n' );
    fprintf ( FId, '    wj = 0.0;\n' );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '\n' );

    % Write Jacobian
    for i = 1 : NumJac;
      UVL_OPT_generateJacobianSubsStr ( FId, dsCostPointMatches{i}, JacNames{i}, Subs, i, NumVars );
    end
 
    fprintf ( FId, '}\n' );

    fclose ( FId );
  else
    fprintf ( 'Error writing file "%s"\n\n', JacobianPointMatchesFileName );
  end

% end
