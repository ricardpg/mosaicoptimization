//
// C++ Automatically generated file using 'UVL_OPT_generateJacobianCamOri.m'
// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>
// Date: 02-Apr-2009 15:50:34
//

#include <mex.h>          // Matlab Mex Functions
#include <cmath>

#define ERROR_HEADER       "MATLAB:UVL_OPT_jacobianCamOriMx.cpp:Input\n"
#define VARIABLES          "Vars = (vxi, vyi, vzi)"

// Identify the input parameters by it's index
enum { VARS=0 };
// Identify the output parameters by it's index
enum { J1=0, J2, J3 };

// Identify elements in Vars vector by it's index
enum { VXI=0, VYI, VZI };

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( nlhs != 3 || nrhs != 1 )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: [j1, j2, j3] = UVL_OPT_jacobianCamOriMx.cpp ( Vars )\n\n"
                   VARIABLES );

  int M = mxGetM ( prhs[VARS] );
  int N = mxGetN ( prhs[VARS] );
  if ( ( M != 1 || N != 3 ) && ( M != 3 || N != 1 ) )
    mexErrMsgTxt ( ERROR_HEADER
                   "Vars must be a 1x3 or a 3x1 vector\n\n"
                   VARIABLES );

  double *Vars = (double *)mxGetPr ( prhs[VARS] );

  double vxi = Vars[VXI];
  double vyi = Vars[VYI];
  double vzi = Vars[VZI];

  double vxi2 = vxi*vxi;
  double vyi4 = vyi*vyi*vyi*vyi;
  double vyi2 = vyi*vyi;
  double vzi2 = vzi*vzi;
  double wi2 = -vxi2-vyi2-vzi2;
  double wi = sqrt(wi2+1.0);
  double vxyi = vxi*vyi;
  double vxzi = vxi*vzi;
  double vyzi = vyi*vzi;

  // Check whether the sqrt() can be computed or not (we have a right vector part of the quaternion!)
  if ( wi2 < -1.0 )
  {
    mexPrintf ( "Point Matches Jacobian cannot be calculated: (wi2 = %f) < 0 in sqrt (wi2)!\n", wi2 );
    wi = 0.0;
  }

  // "x" value in atan ( y / x ) for Roll, Pitch and Yaw
  // to check whether the positive definition or the negative must be used
  double R_x = -sqrt(pow(vxi*vzi*2.0+vyi*wi*2.0,2.0)/(pow(vyi*vzi*2.0-vxi*wi*2.0,2.0)+pow(vxi*vxi+vyi*vyi-vzi*vzi-wi*wi,2.0))+1.0)*(vxi*vxi+vyi*vyi-vzi*vzi-wi*wi);
  double P_x = sqrt(pow(vyi*vzi*2.0-vxi*wi*2.0,2.0)+pow(vxi*vxi+vyi*vyi-vzi*vzi-wi*wi,2.0));
  double Y_x = sqrt(pow(vxi*vzi*2.0+vyi*wi*2.0,2.0)/(pow(vyi*vzi*2.0-vxi*wi*2.0,2.0)+pow(vxi*vxi+vyi*vyi-vzi*vzi-wi*wi,2.0))+1.0)*(vxi*vxi-vyi*vyi-vzi*vzi+wi*wi);

  // Select which are the right Jacobian (Positive or Negative)
  bool SelectJac[6] = { R_x > 0.0, P_x > 0.0, Y_x > 0.0, R_x < 0.0, P_x < 0.0, Y_x < 0.0 };

    // If there is no valid Jacobian => 0
  if ( !SelectJac[0] && !SelectJac[3] )
  {
    mexPrintf ( "Jacobian 1/4 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J1] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J1] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;

    return;
  }

  if ( !SelectJac[1] && !SelectJac[4] )
  {
    mexPrintf ( "Jacobian 2/5 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J2] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J2] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;

    return;
  }

  if ( !SelectJac[2] && !SelectJac[5] )
  {
    mexPrintf ( "Jacobian 3/6 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J3] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J3] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;

    return;
  }

  // Normal Case
  if ( P_x > 1e-8 )
  {
    if ( SelectJac[0] )
    {
      plhs[J1] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j1 = (double *)mxGetPr ( plhs[J1] );
      j1[0] = ((wi*2.0-(vxi2)*1/wi*2.0)/((vxi2)*2.0+(vyi2)*2.0-1.0)-vxi*(vxi*wi*2.0-vyzi*2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0-vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+1.0);
      j1[1] = -((vzi*2.0+vxyi*1/wi*2.0)/((vxi2)*2.0+(vyi2)*2.0-1.0)+vyi*(vxi*wi*2.0-vyzi*2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0-vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+1.0);
      j1[2] = -(vyi*2.0+vxzi*1/wi*2.0)/((pow(vxi*wi*2.0-vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+1.0)*((vxi2)*2.0+(vyi2)*2.0-1.0));
    }

    if ( SelectJac[1] )
    {
      plhs[J2] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j2 = (double *)mxGetPr ( plhs[J2] );
      j2[0] = (vzi-vxyi*1/wi)*1/sqrt((vxi2)*(vyi2)*4.0-(vxi2)*(vzi2)*4.0+(vyi2)*(vzi2)*4.0-(vyi2)*4.0+(vyi4)*4.0-vxyi*vzi*wi*8.0+1.0)*(-2.0);
      j2[1] = -((wi*2.0-(vyi2)*1/wi*2.0)*1/sqrt(pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+pow(vxi*wi*2.0-vyzi*2.0,2.0))+((vzi*2.0+vxyi*1/wi*2.0)*(vxi*wi*2.0-vyzi*2.0)*2.0-vyi*((vxi2)*2.0+(vyi2)*2.0-1.0)*8.0)*1/pow(pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+pow(vxi*wi*2.0-vyzi*2.0,2.0),3.0/2.0)*(vyi*wi*2.0+vxzi*2.0)*(1.0/2.0))/(pow(vyi*wi*2.0+vxzi*2.0,2.0)/(pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+pow(vxi*wi*2.0-vyzi*2.0,2.0))+1.0);
      j2[2] = (vxi-vyzi*1/wi)*1/sqrt((vxi2)*(vyi2)*4.0-(vxi2)*(vzi2)*4.0+(vyi2)*(vzi2)*4.0-(vyi2)*4.0+(vyi4)*4.0-vxyi*vzi*wi*8.0+1.0)*(-2.0);
    }

    if ( SelectJac[2] )
    {
      plhs[J3] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j3 = (double *)mxGetPr ( plhs[J3] );
      j3[0] = -(vyi*2.0+vxzi*1/wi*2.0)/((pow(vzi*wi*2.0-vxyi*2.0,2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0)*((vyi2)*2.0+(vzi2)*2.0-1.0));
      j3[1] = -((vxi*2.0+vyzi*1/wi*2.0)/((vyi2)*2.0+(vzi2)*2.0-1.0)+vyi*(vzi*wi*2.0-vxyi*2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vzi*wi*2.0-vxyi*2.0,2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
      j3[2] = ((wi*2.0-(vzi2)*1/wi*2.0)/((vyi2)*2.0+(vzi2)*2.0-1.0)-vzi*(vzi*wi*2.0-vxyi*2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vzi*wi*2.0-vxyi*2.0,2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
    }

    if ( SelectJac[3] )
    {
      plhs[J1] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j1 = (double *)mxGetPr ( plhs[J1] );
      j1[0] = ((wi*2.0-(vxi2)*1/wi*2.0)/((vxi2)*2.0+(vyi2)*2.0-1.0)-vxi*(vxi*wi*2.0-vyzi*2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0-vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+1.0);
      j1[1] = -((vzi*2.0+vxyi*1/wi*2.0)/((vxi2)*2.0+(vyi2)*2.0-1.0)+vyi*(vxi*wi*2.0-vyzi*2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0-vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+1.0);
      j1[2] = -(vyi*2.0+vxzi*1/wi*2.0)/((pow(vxi*wi*2.0-vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+1.0)*((vxi2)*2.0+(vyi2)*2.0-1.0));
    }

    if ( SelectJac[4] )
    {
      plhs[J2] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j2 = (double *)mxGetPr ( plhs[J2] );
      j2[0] = (vzi-vxyi*1/wi)*1/sqrt((vxi2)*(vyi2)*4.0-(vxi2)*(vzi2)*4.0+(vyi2)*(vzi2)*4.0-(vyi2)*4.0+(vyi4)*4.0-vxyi*vzi*wi*8.0+1.0)*(-2.0);
      j2[1] = -((wi*2.0-(vyi2)*1/wi*2.0)*1/sqrt(pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+pow(vxi*wi*2.0-vyzi*2.0,2.0))+((vzi*2.0+vxyi*1/wi*2.0)*(vxi*wi*2.0-vyzi*2.0)*2.0-vyi*((vxi2)*2.0+(vyi2)*2.0-1.0)*8.0)*1/pow(pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+pow(vxi*wi*2.0-vyzi*2.0,2.0),3.0/2.0)*(vyi*wi*2.0+vxzi*2.0)*(1.0/2.0))/(pow(vyi*wi*2.0+vxzi*2.0,2.0)/(pow((vxi2)*2.0+(vyi2)*2.0-1.0,2.0)+pow(vxi*wi*2.0-vyzi*2.0,2.0))+1.0);
      j2[2] = (vxi-vyzi*1/wi)*1/sqrt((vxi2)*(vyi2)*4.0-(vxi2)*(vzi2)*4.0+(vyi2)*(vzi2)*4.0-(vyi2)*4.0+(vyi4)*4.0-vxyi*vzi*wi*8.0+1.0)*(-2.0);
    }

    if ( SelectJac[5] )
    {
      plhs[J3] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j3 = (double *)mxGetPr ( plhs[J3] );
      j3[0] = -(vyi*2.0+vxzi*1/wi*2.0)/((pow(vzi*wi*2.0-vxyi*2.0,2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0)*((vyi2)*2.0+(vzi2)*2.0-1.0));
      j3[1] = -((vxi*2.0+vyzi*1/wi*2.0)/((vyi2)*2.0+(vzi2)*2.0-1.0)+vyi*(vzi*wi*2.0-vxyi*2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vzi*wi*2.0-vxyi*2.0,2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
      j3[2] = ((wi*2.0-(vzi2)*1/wi*2.0)/((vyi2)*2.0+(vzi2)*2.0-1.0)-vzi*(vzi*wi*2.0-vxyi*2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vzi*wi*2.0-vxyi*2.0,2.0)*1/pow((vyi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
    }

  }
  else
  {
    // Special Case: When P_x is close to 0:
    //               Pitch = 90, Yaw = 0 and Roll = atan2 (...)
    if ( SelectJac[0] )
    {
      plhs[J1] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j1 = (double *)mxGetPr ( plhs[J1] );
      j1[0] = ((wi*2.0-(vxi2)*1/wi*2.0)/((vxi2)*2.0+(vzi2)*2.0-1.0)-vxi*(vxi*wi*2.0+vyzi*2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0+vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
      j1[1] = (vzi*2.0-vxyi*1/wi*2.0)/((pow(vxi*wi*2.0+vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0)*((vxi2)*2.0+(vzi2)*2.0-1.0));
      j1[2] = ((vyi*2.0-vxzi*1/wi*2.0)/((vxi2)*2.0+(vzi2)*2.0-1.0)-vzi*(vxi*wi*2.0+vyzi*2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0+vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
    }

    if ( SelectJac[1] )
    {
      plhs[J2] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j2 = (double *)mxGetPr ( plhs[J2] );
      j2[0] = 0.0;
      j2[1] = 0.0;
      j2[2] = 0.0;
    }

    if ( SelectJac[2] )
    {
      plhs[J3] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j3 = (double *)mxGetPr ( plhs[J3] );
      j3[0] = 0.0;
      j3[1] = 0.0;
      j3[2] = 0.0;
    }

    if ( SelectJac[3] )
    {
      plhs[J1] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j1 = (double *)mxGetPr ( plhs[J1] );
      j1[0] = ((wi*2.0-(vxi2)*1/wi*2.0)/((vxi2)*2.0+(vzi2)*2.0-1.0)-vxi*(vxi*wi*2.0+vyzi*2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0+vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
      j1[1] = (vzi*2.0-vxyi*1/wi*2.0)/((pow(vxi*wi*2.0+vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0)*((vxi2)*2.0+(vzi2)*2.0-1.0));
      j1[2] = ((vyi*2.0-vxzi*1/wi*2.0)/((vxi2)*2.0+(vzi2)*2.0-1.0)-vzi*(vxi*wi*2.0+vyzi*2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)*4.0)/(pow(vxi*wi*2.0+vyzi*2.0,2.0)*1/pow((vxi2)*2.0+(vzi2)*2.0-1.0,2.0)+1.0);
    }

    if ( SelectJac[4] )
    {
      plhs[J2] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j2 = (double *)mxGetPr ( plhs[J2] );
      j2[0] = 0.0;
      j2[1] = 0.0;
      j2[2] = 0.0;
    }

    if ( SelectJac[5] )
    {
      plhs[J3] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
      double *j3 = (double *)mxGetPr ( plhs[J3] );
      j3[0] = 0.0;
      j3[1] = 0.0;
      j3[2] = 0.0;
    }

  }
}
