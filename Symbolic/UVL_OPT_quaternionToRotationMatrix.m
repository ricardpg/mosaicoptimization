%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Salamin '79 convertion from quaternion to rotation matrix.
%
%  File          : UVL_OPT_quaternionToRotationMatrix.m
%  Date          : 27/04/2006 - 27/03/2009
%
%  Compiler      : MATLAB >= 7.7 (R2008b)
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_quaternionToRotationMatrix Convert Quaternion q to a rotation
%  matrix R using Salamin '79 definition.
%
%     Input Parameters:
%      q: 4x1 vector representing a Quaternion.
%
%     Output Parameters:
%      R: 3x3 rotation matrix.
%

function R = UVL_OPT_quaternionToRotationMatrix ( q )
  % Quaternion coefficients
  q0 = q(1);
  qx = q(2);
  qy = q(3);
  qz = q(4);

  % Squared coefficients
  q02 = q0 ^ 2;
  qx2 = qx ^ 2;
  qy2 = qy ^ 2;
  qz2 = qz ^ 2;

  % Cross products
  q0x = 2.0 * q0 * qx;
  q0y = 2.0 * q0 * qy;
  q0z = 2.0 * q0 * qz;
  qxy = 2.0 * qx * qy;
  qxz = 2.0 * qx * qz;
  qyz = 2.0 * qy * qz;

  R = [ q02+qx2-qy2-qz2         -q0z+qxy          q0y+qxz;
                q0z+qxy  q02-qx2+qy2-qz2         -q0x+qyz;
               -q0y+qxz          q0x+qyz  q02-qx2-qy2+qz2 ];
end
