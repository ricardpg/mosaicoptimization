function UVL_OPT_generateJacobianSubsStr ( FID, Cost, JacName, Subs, NumJac, NumVars )
  % Test the input parameters
  error ( nargchk ( 6, 6, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  fprintf ( 'Writing Jacobian for %s...\n', JacName );
  fprintf ( FID, '  // Jacobian for %s\n', JacName );
  fprintf ( FID, '  plhs[J%d] = mxCreateDoubleMatrix ( %d, 1, mxREAL );\n',  NumJac, NumVars );
  fprintf ( FID, '  double *j%d = (double *)mxGetPr ( plhs[J%d] );\n', NumJac, NumJac );
  for j = 1 : NumVars;
    fprintf ( '  Writing Jacobian %d/%d...\n', j, NumVars );
    StrRes = strtrim ( ccode ( Cost(1,j) ) );
    StrRes = UVL_OPT_subsStr ( StrRes, Subs );
    StrRes = strrep ( StrRes, 't0 =', sprintf ( 'j%d[%d] =', NumJac, j - 1 ) );
    fprintf ( FID, '  %s\n', StrRes );
  end
  fprintf ( FID, '\n' );
end
