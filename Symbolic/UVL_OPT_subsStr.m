function Str = UVL_OPT_subsStr ( Str, SubStr )
  for i = 1 : size ( SubStr, 2 );
    Str = strrep ( Str, SubStr{1,i}, SubStr{2,i} );
  end
end
