%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobian Computation in C++ code.
%
%  File          : UVL_OPT_calculateJacobianCamOriStr.m
%  Date          : 26/04/2006 - 02/04/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_calculateJacobianCamOriStr Calculate the C/C++ for the symbolic
%                                     expressions defined in the input cell-array.
%                                     The code is done for the particular
%                                     case of the Jacobian of atan2() for angular
%                                     readings for which two definitions
%                                     (postive and negative) are needed.
%                                     SelectedJac is a boolean array used
%                                     to activate the positive or the
%                                     negative Jacobian.
%
%     Input Parameters:
%      FID: Opened Output File Identifier.
%      CostFunc: Cell-Array containing the Jacobian to be converted to C/C++.
%      Subs: Cell-Array containing the variable substitutions.
%      NumVars: Number of variables of the function.
%
%     Output Parameters:
%

function UVL_OPT_calculateJacobianCamOriStr ( FID, CostFunc, Subs, NumVars )
  % Test the input parameters
  error ( nargchk ( 4, 4, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );
  
  % Number of functions to be computed
  NumJac = size ( CostFunc, 1 );

  % Functions are the half of the input size of the Cell-Array
  % (positive/negative)
  RealJac = NumJac / 2;

  % Scan the input functions
  for i = 1:NumJac;
    NJac = mod ( i-1, RealJac ) + 1;
      
    fprintf ( FID, '    if ( SelectJac[%d] )\n', i - 1 );
    fprintf ( FID, '    {\n' );

    fprintf ( FID, '      plhs[J%d] = mxCreateDoubleMatrix ( %d, 1, mxREAL );\n', NJac, NumVars );
    fprintf ( FID, '      double *j%d = (double *)mxGetPr ( plhs[J%d] );\n', NJac, NJac );

    for j = 1:size ( CostFunc{i}, 2 );
      StrRes = strtrim ( ccode ( CostFunc{i}(1,j) ) );
      StrRes = UVL_OPT_subsStr ( StrRes, Subs );
      StrRes = strrep ( StrRes, 't0 =', sprintf ( 'j%d[%d] =', NJac, j - 1 ) );
      fprintf ( FID, '      %s\n', StrRes );
    end

    fprintf ( FID, '    }\n' );
    
    fprintf ( FID, '\n' );
  end
end
