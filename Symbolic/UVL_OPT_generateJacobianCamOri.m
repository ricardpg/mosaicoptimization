%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobian for Camera Orientation Readings Algebraically.
%
%  File          : UVL_OPT_generateJacobianCamOri.m
%  Date          : 27/04/2006 - 02/04/2009
%
%  Compiler      : MATLAB >= 7.7 (R2008b)
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_generateJacobianCamOri Calculate the Jacobian for the Camera Orientation Reading
%                                 data algebraically. The function creates a Matlab's
%                                 C++ Mex file.
%
%     Input Parameters:
%      JacobianCamOriFileName: String containing the C++ generated file name. 
%                              By default is "UVL_OPT_jacobianCamOriMx.cpp".
%
%     Output Parameters:
%

% function UVL_OPT_generateJacobianCamOri ( JacobianCamOriFileName )
%   % Test the input parameters
%   error ( nargchk ( 0, 1, nargin ) );
%   error ( nargoutchk ( 0, 0, nargout ) );
%
%   % Default value for the output filename
%   if nargin < 1 
%     JacobianCamOriFileName = 'JacobianCamOriMx.cpp';
%   end

  if ~exist ( 'JacobianCamOriFileName', 'var' );
    JacobianCamOriFileName = 'UVL_OPT_jacobianCamOriMx.cpp';
  end

  % Flags for simplifying equations
  DoSimplifyw = false;
  DoSimplifyCosts = true;
  DoSimplifyJacobian = true;

  disp ( 'Defining symbolic variables...' );

  % Symbolic Quaternion Fields for Image i
  vxi = sym ( 'vxi', 'real' );
  vyi = sym ( 'vyi', 'real' );
  vzi = sym ( 'vzi', 'real' );
  vwi = ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2);
  % Scalar from vector
  if DoSimplifyw;
    wi = vwi;
  else
    wi = sym ( 'wi', 'real' );
  end

  % Symblic Elements of Rotation Matrices Represented by Quaternions for Image i
  Ri = UVL_OPT_quaternionToRotationMatrix ( [ wi vxi vyi vzi ] );

  % Symbolic Roll, Pitch and Yaw.
  P = sym ( 'P', 'real' );
  Y = sym ( 'Y', 'real' );
  R = sym ( 'R', 'real' );

  % Definition of atan2 according to FDLIBM www.netlib.org ( IEEE 754 )
  %   http://www.netlib.org/fdlibm/e_atan2.c
  %                    /  atan ( y / x )         if x > 0
  %   atan2 ( y, x ) = |
  %                    \  pi - atan ( y / -x )   if x < 0

  % Definitions according to: Rxyz = Rx ( Roll ) * Ry ( Pitch ) * Rz ( Yaw )
  %

  % Px = sqrt ( R(2,3)*R(2,3) + R(3,3)*R(3,3) );
  %
  % if Px > 1e-8;
  %   Pitch = atan2 ( R(1,3), Px );
  %   cP = cos ( Pitch );
  %   Yaw = atan2 ( -R(1,2)/cP, R(1,1)/cP );
  %   Roll = atan2 ( -R(2,3)/cP, R(3,3)/cP );
  % else
  %   Pitch = pi / 2;
  %   Yaw = 0;
  %   Roll = atan2 ( R(3,2), R(2,2) );
  % end

  % Positive Definitions 
  P_x = sqrt ( Ri(2,3)*Ri(2,3) + Ri(3,3)*Ri(3,3) );
  P_p = atan ( Ri(1,3) / P_x );
  Y_x = Ri(1,1) / cos ( P_p );
  Y_p = atan ( -Ri(1,2) / Ri(1,1) );
  R_x = Ri(3,3) / cos ( P_p );
  R_p = atan ( -Ri(2,3) / Ri(3,3) );

  % Negative Definitions
  P_n = pi - atan ( -Ri(1,3) / P_x );
  Y_n = pi - atan ( Ri(1,2) / Ri(1,1) );
  R_n = pi - atan ( Ri(2,3) / Ri(3,3) );
  
  % Special Case: When P_x is close to 0 => Pitch = 90, Yaw = 0 and Roll = atan2
  P2_p = pi / 2;
  Y2_p = 0;
  R2_p = atan ( Ri(3,2) / Ri(2,2) );

  P2_n = pi / 2;
  Y2_n = 0;
  R2_n = pi - atan ( -Ri(3,2) / Ri(2,2) );
  
  disp ( 'Calculating Residuals for Camera Orientation Readings (Roll, Pitch, Yaw)...' );
  % Residuals definitions according to positive definition
  CostR_p = R - R_p;
  CostP_p = P - P_p;
  CostY_p = Y - Y_p;
  
  % Residuals definitions according to negative definition
  CostR_n = R - R_n;
  CostP_n = P - P_n;
  CostY_n = Y - Y_n;

  % Special case (Pitch Singularity)
  CostR2_p = R - R2_p;
  CostP2_p = P - P2_p;
  CostY2_p = Y - Y2_p;
  
  CostR2_n = R - R2_n;
  CostP2_n = P - P2_n;
  CostY2_n = Y - Y2_n;

  if DoSimplifyCosts;
    disp ( 'Simplifying Cost Expressions...' );
    % Positive Definitions
    disp ( '  Positive definitions...' );
    disp ( '    Normal case...' );
    sCostR_p = simplify ( CostR_p );
    sCostP_p = simplify ( CostP_p );
    sCostY_p = simplify ( CostY_p );
    disp ( '    Pitch singularity case...' );
    sCostR2_p = simplify ( CostR2_p );
    sCostP2_p = simplify ( CostP2_p );
    sCostY2_p = simplify ( CostY2_p );

    % Negative Definitions
    disp ( '  Simplifying negative definitions...' );
    disp ( '    Normal case...' );
    sCostR_n = simplify ( CostR_n );
    sCostP_n = simplify ( CostP_n );
    sCostY_n = simplify ( CostY_n );
    disp ( '    Pitch singularity case...' );
    sCostR2_n = simplify ( CostR2_n );
    sCostP2_n = simplify ( CostP2_n );
    sCostY2_n = simplify ( CostY2_n );
  else
    % Positive Definitions
    % Normal Case
    sCostR_p = CostR_p;
    sCostP_p = CostP_p;
    sCostY_p = CostY_p;

    % Pitch Singularity Case
    sCostR2_p = CostR2_p;
    sCostP2_p = CostP2_p;
    sCostY2_p = CostY2_p;

    % Negative Definitions
    % Normal Case
    sCostR_n = CostR_n;
    sCostP_n = CostP_n;
    sCostY_n = CostY_n;

    % Pitch Singularity Case
    sCostR2_n = CostR2_n;
    sCostP2_n = CostP2_n;
    sCostY2_n = CostY2_n;
  end

  % Sizes
  NumJac = 3;
  NumVars = 3;
  JacNames = cell ( NumJac, 1 );
  JacNames{1} = 'Roll';
  JacNames{2} = 'Pitch';
  JacNames{3} = 'Yaw';

  % Allocate Cell-Array
  dCostCamOri = cell ( NumJac*2, 1 );
  dCostCamOri2 = cell ( NumJac*2, 1 );
  dsCostCamOri = cell ( NumJac*2, 1 );
  dsCostCamOri2 = cell ( NumJac*2, 1 );

  % Replace wi and wj for its value
  if ~DoSimplifyw;
    % Normal Case
    % Positive Definitions
    sCostR_p = subs ( sCostR_p, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostP_p = subs ( sCostP_p, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostY_p = subs ( sCostY_p, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );

    % Pitch Singularity Case
    sCostR_n = subs ( sCostR_n, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostP_n = subs ( sCostP_n, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostY_n = subs ( sCostY_n, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );

    % Negative Definitions
    % Normal Case
    sCostR2_p = subs ( sCostR2_p, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostP2_p = subs ( sCostP2_p, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostY2_p = subs ( sCostY2_p, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );

    % Pitch Singularity Case
    sCostR2_n = subs ( sCostR2_n, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostP2_n = subs ( sCostP2_n, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostY2_n = subs ( sCostY2_n, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
  end

  % Jacobian of the cost definitions
  % Positive Definitions
  % Normal Case
  dCostCamOri{1} = jacobian ( sCostR_p, [vxi, vyi, vzi] );
  dCostCamOri{2} = jacobian ( sCostP_p, [vxi, vyi, vzi] );
  dCostCamOri{3} = jacobian ( sCostY_p, [vxi, vyi, vzi] );

  % Pitch Singularity Case
  dCostCamOri{4} = jacobian ( sCostR_n, [vxi, vyi, vzi] );
  dCostCamOri{5} = jacobian ( sCostP_n, [vxi, vyi, vzi] );
  dCostCamOri{6} = jacobian ( sCostY_n, [vxi, vyi, vzi] );

  % Negative Definitions
  % Normal Case
  dCostCamOri2{1} = jacobian ( sCostR2_p, [vxi, vyi, vzi] );
  dCostCamOri2{2} = jacobian ( sCostP2_p, [vxi, vyi, vzi] );
  dCostCamOri2{3} = jacobian ( sCostY2_p, [vxi, vyi, vzi] );
  
  % Pitch Singularity Case
  dCostCamOri2{4} = jacobian ( sCostR2_n, [vxi, vyi, vzi] );
  dCostCamOri2{5} = jacobian ( sCostP2_n, [vxi, vyi, vzi] );
  dCostCamOri2{6} = jacobian ( sCostY2_n, [vxi, vyi, vzi] );

  if DoSimplifyJacobian;
    for j = 1 : NumJac * 2;
      if j <= NumJac
        DefStr = 'Positive';
      else
        DefStr = 'Negative';
      end
      fprintf ( 'Simplifying Jacobian for %s in %s definition...\n', JacNames{fix(mod(j-1,NumJac)+1)}, DefStr );
      for i = 1 : NumVars;
        fprintf ( '  Simplifying Jacobian %d/%d\n', i, NumVars );
        disp ( '    Normal Case' );
        dsCostCamOri{j}(1,i) = simplify ( dCostCamOri{j}(1,i) );
        disp ( '    Pitch Singularity Case' );
        dsCostCamOri2{j}(1,i) = simplify ( dCostCamOri2{j}(1,i) );
      end
    end
  else
    for i = 1 : NumJac * 2;
      dsCostCamOri{i} = dCostCamOri{i};
      dsCostCamOri2{i} = dCostCamOri2{i};
    end
  end

  % Substitutions
  Subs = cell ( 2, 0 );
  i = 1;
  Subs{1,i} = 'vxi*vxi';                    Subs{2,i} = 'vxi2';  i = i + 1;
  Subs{1,i} = 'vyi*vyi*vyi*vyi';            Subs{2,i} = 'vyi4';  i = i + 1;
  Subs{1,i} = 'vyi*vyi';                    Subs{2,i} = 'vyi2';  i = i + 1;
  Subs{1,i} = 'vzi*vzi';                    Subs{2,i} = 'vzi2';  i = i + 1;
  Subs{1,i} = '-vxi2-vyi2-vzi2';            Subs{2,i} = 'wi2';   i = i + 1;
  Subs{1,i} = 'sqrt(wi2+1.0)';              Subs{2,i} = 'wi';    i = i + 1;

  Subs{1,i} = 'vxi*vyi';                    Subs{2,i} = 'vxyi';  i = i + 1;
  Subs{1,i} = 'vxi*vzi';                    Subs{2,i} = 'vxzi';  i = i + 1;
  Subs{1,i} = 'vyi*vzi';                    Subs{2,i} = 'vyzi';  i = i + 1;

  % Create the output Mex file
  FId = fopen ( JacobianCamOriFileName, 'w' );
  if FId ~= -1;
    fprintf ( 'Generating "%s" Mex File...\n', JacobianCamOriFileName );

    fprintf ( FId, '//\n' );
    fprintf ( FId, '// C++ Automatically generated file using ''%s.m''\n', mfilename );
    fprintf ( FId, '// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>\n' );
    fprintf ( FId, '// Date: %s\n', datestr ( now ) );
    fprintf ( FId, '//\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#include <mex.h>          // Matlab Mex Functions\n' );
    fprintf ( FId, '#include <cmath>\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#define ERROR_HEADER       "MATLAB:%s:Input\\n"\n', JacobianCamOriFileName );
    fprintf ( FId, '#define VARIABLES          "Vars = (vxi, vyi, vzi)"\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify the input parameters by it''s index\n' );
    fprintf ( FId, 'enum { VARS=0 };\n' );

    fprintf ( FId, '// Identify the output parameters by it''s index\n' );
    fprintf ( FId, 'enum { J1=0, J2, J3 };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify elements in Vars vector by it''s index\n' );
    fprintf ( FId, 'enum { VXI=0, VYI, VZI };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, 'void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )\n' );
    fprintf ( FId, '{\n' );
    fprintf ( FId, '  // Parameter checks\n');
    fprintf ( FId, '  if ( nlhs != 3 || nrhs != 1 )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Usage: [j1, j2, j3] = %s ( Vars )\\n\\n"\n', JacobianCamOriFileName );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  int M = mxGetM ( prhs[VARS] );\n' );
    fprintf ( FId, '  int N = mxGetN ( prhs[VARS] );\n' );
    fprintf ( FId, '  if ( ( M != 1 || N != 3 ) && ( M != 3 || N != 1 ) )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Vars must be a 1x3 or a 3x1 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *Vars = (double *)mxGetPr ( prhs[VARS] );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double vxi = Vars[VXI];\n' );
    fprintf ( FId, '  double vyi = Vars[VYI];\n' );
    fprintf ( FId, '  double vzi = Vars[VZI];\n' );
    fprintf ( FId, '\n' );

    % Perform substitutions
    for i = 1 : size ( Subs, 2 );
      fprintf ( FId, '  double %s = %s;\n', Subs{2,i}, Subs{1,i} );
    end
    fprintf ( FId, '\n' );

    % Check for the negative square root
    fprintf ( FId, '  // Check whether the sqrt() can be computed or not (we have a right vector part of the quaternion!)\n' );
    fprintf ( FId, '  if ( wi2 < -1.0 )\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    mexPrintf ( "Point Matches Jacobian cannot be calculated: (wi2 = %%f) < 0 in sqrt (wi2)!\\n", wi2 );\n' );
    fprintf ( FId, '    wi = 0.0;\n' );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '\n' );
    
    fprintf ( FId, '  // "x" value in atan ( y / x ) for Roll, Pitch and Yaw\n' );
    fprintf ( FId, '  // to check whether the positive definition or the negative must be used\n' );    
    R_s = strtrim ( ccode ( R_x ) );
    R_s = strrep ( R_s, 't0 =', 'double R_x =' );
    fprintf ( FId, '  %s\n', R_s );

    P_s = strtrim ( ccode ( P_x ) );
    P_s = strrep ( P_s, 't0 =', 'double P_x =' );
    fprintf ( FId, '  %s\n', P_s );

    Y_s = strtrim ( ccode ( Y_x ) );
    Y_s = strrep ( Y_s, 't0 =', 'double Y_x =' );
    fprintf ( FId, '  %s\n', Y_s );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  // Select which are the right Jacobian (Positive or Negative)\n' );
    fprintf ( FId, '  bool SelectJac[%d] = { R_x > 0.0, P_x > 0.0, Y_x > 0.0, R_x < 0.0, P_x < 0.0, Y_x < 0.0 };\n', NumJac*2 );
    fprintf ( FId, '\n' );

    % If the x is 0 => the atan cannot be computed => Jacobian = 0
    fprintf ( FId, '    // If there is no valid Jacobian => 0\n' );
    for i = 1 : NumJac;
      fprintf ( FId, '  if ( !SelectJac[%d] && !SelectJac[%d] )\n', i - 1, NumJac + i - 1 );
      fprintf ( FId, '  {\n' );
      fprintf ( FId, '    mexPrintf ( "Jacobian %d/%d cannot be calculated: x = 0 in atan (y / x)!\\n" );\n', i, i + NumJac );
      fprintf ( FId, '\n' );
      fprintf ( FId, '    plhs[J%d] = mxCreateDoubleMatrix ( %d, 1, mxREAL );\n', i, NumVars );
      fprintf ( FId, '    double *j = (double *)mxGetPr ( plhs[J%d] );\n', i );
      for j = 1 : NumVars;
        fprintf ( FId, '    j[%d] = 0.0;\n', j - 1 );
      end
      fprintf ( FId, '\n' );
      fprintf ( FId, '    return;\n' );
      fprintf ( FId, '  }\n' );
      fprintf ( FId, '\n' );
    end
  
    fprintf ( FId, '  // Normal Case\n' );
    fprintf ( FId, '  if ( P_x > 1e-8 )\n' );
    fprintf ( FId, '  {\n' );
    % Write Jacobian
    UVL_OPT_calculateJacobianCamOriStr ( FId, dsCostCamOri, Subs, NumVars );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '  else\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    // Special Case: When P_x is close to 0:\n' );
    fprintf ( FId, '    //               Pitch = 90, Yaw = 0 and Roll = atan2 (...)\n' );
    % Write Jacobian
    UVL_OPT_calculateJacobianCamOriStr ( FId, dsCostCamOri2, Subs, NumVars );
    fprintf ( FId, '  }\n' );

    fprintf ( FId, '}\n' );

    fclose ( FId );
  else
    fprintf ( 'Error writing file "%s"\n', JacobianCamOriFileName );
  end

% end
