//
// C++ Automatically generated file using 'UVL_OPT_generateJacobianRefPoint.m'
// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>
// Date: 03-Apr-2009 12:06:24
//

#include <mex.h>          // Matlab Mex Functions
#include <cmath>

#define ERROR_HEADER       "MATLAB:UVL_OPT_jacobianRefPointMx.cpp:Input\n"
#define VARIABLES          "Vars = (au, av, cx, cy, vxi, vyi, vzi, txi, tyi, tzi, mx, my)"

// Identify the input parameters by it's index
enum { VARS=0 };
// Identify the output parameters by it's index
enum { J1=0, J2 };

// Identify elements in Vars vector by it's index
enum { AU=0, AV, CX, CY, VXI, VYI, VZI, TXI, TYI, TZI, MX, MY };

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( nlhs != 2 || nrhs != 1 )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: [j1, j2] = UVL_OPT_jacobianRefPointMx.cpp ( Vars )\n\n"
                   VARIABLES );

  int M = mxGetM ( prhs[VARS] );
  int N = mxGetN ( prhs[VARS] );
  if ( ( M != 1 || N != 12 ) && ( M != 12 || N != 1 ) )
    mexErrMsgTxt ( ERROR_HEADER
                   "Vars must be a 1x12 or a 12x1 vector\n\n"
                   VARIABLES );

  double *Vars = (double *)mxGetPr ( prhs[VARS] );

  double au  = Vars[AU];
  double av  = Vars[AV];
  double cx  = Vars[CX];
  double cy  = Vars[CY];
  double vxi = Vars[VXI];
  double vyi = Vars[VYI];
  double vzi = Vars[VZI];
  double txi = Vars[TXI];
  double tyi = Vars[TYI];
  double tzi = Vars[TZI];
  double mx  = Vars[MX];
  double my  = Vars[MY];

  double vxi2 = vxi*vxi;
  double vyi4 = vyi*vyi*vyi*vyi;
  double vyi2 = vyi*vyi;
  double vzi3 = vzi*vzi*vzi;
  double vzi2 = vzi*vzi;
  double wi2 = -vxi2-vyi2-vzi2;
  double wi = sqrt(wi2+1.0);
  double vxyi = vxi*vyi;
  double vxzi = vxi*vzi;
  double vyzi = vyi*vzi;

  // Check whether the sqrt() can be computed or not (we have a right vector part of the quaternion!)
  if ( wi2 < -1.0 )
  {
    mexPrintf ( "Point Matches Jacobian cannot be calculated: (wi2 = %f) < 0 in sqrt (wi2)!\n", wi2 );
    wi = 0.0;
  }

  // Jacobian for x
  plhs[J1] = mxCreateDoubleMatrix ( 6, 1, mxREAL );
  double *j1 = (double *)mxGetPr ( plhs[J1] );
  j1[0] = -(tzi*(cx*vxi*4.0-au*(vzi*2.0-vxyi*1/wi*2.0))+my*(cx*(wi*2.0-(vxi2)*1/wi*2.0)+au*(vyi*2.0+vxzi*1/wi*2.0))-tyi*(cx*(wi*2.0-(vxi2)*1/wi*2.0)+au*(vyi*2.0+vxzi*1/wi*2.0))+cx*mx*(vzi*2.0+vxyi*1/wi*2.0)-cx*txi*(vzi*2.0+vxyi*1/wi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(tzi*vxi*4.0+my*(wi*2.0-(vxi2)*1/wi*2.0)-tyi*(wi*2.0-(vxi2)*1/wi*2.0)+mx*(vzi*2.0+vxyi*1/wi*2.0)-txi*(vzi*2.0+vxyi*1/wi*2.0))*(-mx*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+txi*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+tzi*(cx*((vxi2)*2.0+(vyi2)*2.0-1.0)-au*(vyi*wi*2.0+vxzi*2.0))-my*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0))+tyi*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j1[1] = -(my*(au*(vxi*2.0+vyzi*1/wi*2.0)+cx*(vzi*2.0-vxyi*1/wi*2.0))-tyi*(au*(vxi*2.0+vyzi*1/wi*2.0)+cx*(vzi*2.0-vxyi*1/wi*2.0))-mx*(au*vyi*4.0+cx*(wi*2.0-(vyi2)*1/wi*2.0))+txi*(au*vyi*4.0+cx*(wi*2.0-(vyi2)*1/wi*2.0))+tzi*(cx*vyi*4.0-au*(wi*2.0-(vyi2)*1/wi*2.0)))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(tzi*vyi*4.0-mx*(wi*2.0-(vyi2)*1/wi*2.0)+txi*(wi*2.0-(vyi2)*1/wi*2.0)+my*(vzi*2.0-vxyi*1/wi*2.0)-tyi*(vzi*2.0-vxyi*1/wi*2.0))*(-mx*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+txi*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+tzi*(cx*((vxi2)*2.0+(vyi2)*2.0-1.0)-au*(vyi*wi*2.0+vxzi*2.0))-my*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0))+tyi*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j1[2] = (mx*(au*vzi*4.0-cx*(vxi*2.0+vyzi*1/wi*2.0))-txi*(au*vzi*4.0-cx*(vxi*2.0+vyzi*1/wi*2.0))+my*(au*(wi*2.0-(vzi2)*1/wi*2.0)-cx*(vyi*2.0-vxzi*1/wi*2.0))-tyi*(au*(wi*2.0-(vzi2)*1/wi*2.0)-cx*(vyi*2.0-vxzi*1/wi*2.0))+au*tzi*(vxi*2.0-vyzi*1/wi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(mx*(vxi*2.0+vyzi*1/wi*2.0)+my*(vyi*2.0-vxzi*1/wi*2.0)-txi*(vxi*2.0+vyzi*1/wi*2.0)-tyi*(vyi*2.0-vxzi*1/wi*2.0))*(-mx*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+txi*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+tzi*(cx*((vxi2)*2.0+(vyi2)*2.0-1.0)-au*(vyi*wi*2.0+vxzi*2.0))-my*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0))+tyi*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j1[3] = -(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(vyi*wi*2.0-vxzi*2.0)*(-mx*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+txi*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+tzi*(cx*((vxi2)*2.0+(vyi2)*2.0-1.0)-au*(vyi*wi*2.0+vxzi*2.0))-my*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0))+tyi*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j1[4] = -(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))-(vxi*wi*2.0+vyzi*2.0)*(-mx*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+txi*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+tzi*(cx*((vxi2)*2.0+(vyi2)*2.0-1.0)-au*(vyi*wi*2.0+vxzi*2.0))-my*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0))+tyi*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j1[5] = -(cx*((vxi2)*2.0+(vyi2)*2.0-1.0)-au*(vyi*wi*2.0+vxzi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+((vxi2)*2.0+(vyi2)*2.0-1.0)*(-mx*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+txi*(au*((vyi2)*2.0+(vzi2)*2.0-1.0)+cx*(vyi*wi*2.0-vxzi*2.0))+tzi*(cx*((vxi2)*2.0+(vyi2)*2.0-1.0)-au*(vyi*wi*2.0+vxzi*2.0))-my*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0))+tyi*(au*(vzi*wi*2.0-vxyi*2.0)-cx*(vxi*wi*2.0+vyzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);

  // Jacobian for y
  plhs[J2] = mxCreateDoubleMatrix ( 6, 1, mxREAL );
  double *j2 = (double *)mxGetPr ( plhs[J2] );
  j2[0] = -(mx*(av*(vyi*2.0-vxzi*1/wi*2.0)+cy*(vzi*2.0+vxyi*1/wi*2.0))-txi*(av*(vyi*2.0-vxzi*1/wi*2.0)+cy*(vzi*2.0+vxyi*1/wi*2.0))-my*(av*vxi*4.0-cy*(wi*2.0-(vxi2)*1/wi*2.0))+tyi*(av*vxi*4.0-cy*(wi*2.0-(vxi2)*1/wi*2.0))+tzi*(cy*vxi*4.0+av*(wi*2.0-(vxi2)*1/wi*2.0)))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(tzi*vxi*4.0+my*(wi*2.0-(vxi2)*1/wi*2.0)-tyi*(wi*2.0-(vxi2)*1/wi*2.0)+mx*(vzi*2.0+vxyi*1/wi*2.0)-txi*(vzi*2.0+vxyi*1/wi*2.0))*(-my*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+tzi*(cy*((vxi2)*2.0+(vyi2)*2.0-1.0)+av*(vxi*wi*2.0-vyzi*2.0))+tyi*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+mx*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0))-txi*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j2[1] = -(tzi*(cy*vyi*4.0-av*(vzi*2.0+vxyi*1/wi*2.0))-mx*(cy*(wi*2.0-(vyi2)*1/wi*2.0)-av*(vxi*2.0-vyzi*1/wi*2.0))+txi*(cy*(wi*2.0-(vyi2)*1/wi*2.0)-av*(vxi*2.0-vyzi*1/wi*2.0))+cy*my*(vzi*2.0-vxyi*1/wi*2.0)-cy*tyi*(vzi*2.0-vxyi*1/wi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(tzi*vyi*4.0-mx*(wi*2.0-(vyi2)*1/wi*2.0)+txi*(wi*2.0-(vyi2)*1/wi*2.0)+my*(vzi*2.0-vxyi*1/wi*2.0)-tyi*(vzi*2.0-vxyi*1/wi*2.0))*(-my*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+tzi*(cy*((vxi2)*2.0+(vyi2)*2.0-1.0)+av*(vxi*wi*2.0-vyzi*2.0))+tyi*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+mx*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0))-txi*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j2[2] = (my*(av*vzi*4.0-cy*(vyi*2.0-vxzi*1/wi*2.0))-tyi*(av*vzi*4.0-cy*(vyi*2.0-vxzi*1/wi*2.0))-mx*(av*(wi*2.0-(vzi2)*1/wi*2.0)+cy*(vxi*2.0+vyzi*1/wi*2.0))+txi*(av*(wi*2.0-(vzi2)*1/wi*2.0)+cy*(vxi*2.0+vyzi*1/wi*2.0))+av*tzi*(vyi*2.0+vxzi*1/wi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(mx*(vxi*2.0+vyzi*1/wi*2.0)+my*(vyi*2.0-vxzi*1/wi*2.0)-txi*(vxi*2.0+vyzi*1/wi*2.0)-tyi*(vyi*2.0-vxzi*1/wi*2.0))*(-my*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+tzi*(cy*((vxi2)*2.0+(vyi2)*2.0-1.0)+av*(vxi*wi*2.0-vyzi*2.0))+tyi*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+mx*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0))-txi*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j2[3] = (av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+(vyi*wi*2.0-vxzi*2.0)*(-my*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+tzi*(cy*((vxi2)*2.0+(vyi2)*2.0-1.0)+av*(vxi*wi*2.0-vyzi*2.0))+tyi*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+mx*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0))-txi*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j2[4] = -(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))-(vxi*wi*2.0+vyzi*2.0)*(-my*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+tzi*(cy*((vxi2)*2.0+(vyi2)*2.0-1.0)+av*(vxi*wi*2.0-vyzi*2.0))+tyi*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+mx*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0))-txi*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);
  j2[5] = -(cy*((vxi2)*2.0+(vyi2)*2.0-1.0)+av*(vxi*wi*2.0-vyzi*2.0))/(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0))+((vxi2)*2.0+(vyi2)*2.0-1.0)*(-my*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+tzi*(cy*((vxi2)*2.0+(vyi2)*2.0-1.0)+av*(vxi*wi*2.0-vyzi*2.0))+tyi*(av*((vxi2)*2.0+(vzi2)*2.0-1.0)-cy*(vxi*wi*2.0+vyzi*2.0))+mx*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0))-txi*(av*(vzi*wi*2.0+vxyi*2.0)-cy*(vyi*wi*2.0-vxzi*2.0)))*1/pow(tzi*((vxi2)*2.0+(vyi2)*2.0-1.0)-mx*(vyi*wi*2.0-vxzi*2.0)+my*(vxi*wi*2.0+vyzi*2.0)+txi*(vyi*wi*2.0-vxzi*2.0)-tyi*(vxi*wi*2.0+vyzi*2.0),2.0);

}
