%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Generate and Compile Jacobian Functions.
%
%  File          : UVL_OPT_compile.m
%  Date          : 20/12/2007 - 19/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - This script should be run in Matlab from the
%                    MosaicOptimization/Symbolic/ folder.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%


% Detect Matlab Version
MatVer = version ( '-release' );

% 13 for 6.5 or smaller for lower versions and 20 for others (2006a, 2006b, 2007a, 2007b)
Major = str2double ( MatVer(1:2) );

% Flags
if ~exist ( 'DoGenerateJacobian', 'var' ); DoGenerateJacobian = false; end;
if ~exist ( 'DoCompileJacobian', 'var' ); DoCompileJacobian = true; end;
if ~exist ( 'DoMoveJacobian', 'var' ); DoMoveJacobian = false; end;

% Perform Version Check
if Major < 20;
  CheckPassed = false;
else
  Minor = str2double ( MatVer(3:4) );
  Alpha = double ( MatVer(5) );
  if Minor < 8;
    CheckPassed = false;
  else
    if Alpha < double ( 'b' );
      CheckPassed = false;
    else
      CheckPassed = true;
    end
  end
end

if ~CheckPassed;
  error ( 'MATLAB:UVL_OPT_Compile:Check', ...
          'Your Matlab version ''%s'' is smaller than 2008b.\nGeneration and Compilation will not work!\n', MatVer );
end

% Destination Folder
DstFolder = '..';

% Different types of Jacobian
JacobianType{1} = 'PointMatches';
JacobianType{2} = 'RefPoint';
JacobianType{3} = 'CamOri';

% Process all Files
for i = 1:3
  fprintf ('Processing ''%s''...\n', JacobianType{i} );
  CppFileName = [ 'UVL_OPT_jacobian' JacobianType{i} 'Mx' ];
  FileNameSrc = [ CppFileName '.cpp' ];
  FileNameMex = [ CppFileName '.' mexext ];

  % Generate Jacobian
  if DoGenerateJacobian;
    fprintf ( '  Generating Mex File ''%s'':\n', FileNameSrc );
    Cmd = [ 'UVL_OPT_generateJacobian' JacobianType{i} ' ( FileNameSrc );' ];
    fprintf ( '    >> %s\n', Cmd );
    eval ( Cmd );
  else
    fprintf ( '  Skiping generation of Mex File ''%s''!\n', FileNameSrc );
  end
    
  % Compile Jacobian
  if DoCompileJacobian;
    fprintf ( '  Compiling Mex File ''%s''...\n', FileNameSrc );
    Cmd = [ 'mex ' FileNameSrc ];
    fprintf ( '    >> %s\n', Cmd );
    eval ( Cmd );
  else
    fprintf ( '  Skiping compilation Mex File ''%s''!\n', FileNameSrc );
  end

  % Move Jacobian
  if DoMoveJacobian;
    fprintf ( '  Moving ''%s'' -> ''%s/%s''...\n',  FileNameMex, DstFolder, FileNameMex );
    movefile ( FileNameMex, DstFolder );
  else
    fprintf ( '  Skiping move ''%s'' -> ''%s/%s''!\n',  FileNameMex, DstFolder, FileNameMex );
  end

  fprintf ( '\n' );
end
