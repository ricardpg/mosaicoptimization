%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobian for Reference (Fiducial) Point Readings Algebraically.
%
%  File          : UVL_OPT_generateJacobianRefPoint.m
%  Date          : 26/04/2006 - 03/04/2009
%
%  Compiler      : MATLAB >= 7.7 (R2008b)
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  UVL_OPT_generateJacobianRefPoint Calculate the Jacobian for the Reference
%                                   (Fiducial) Point Reading data algebraically.
%                                   The function creates a Matlab's C++ Mex file.
%
%     Input Parameters:
%      JacobianRefPointFileName: String containing the C++ generated file name. 
%                                By default is "UVL_OPT_jacobianRefPointMx.cpp".
%
%     Output Parameters:
%

% function UVL_OPT_generateJacobianRefPoint ( JacobianRefPointFileName )
%   % Test the input parameters
%   error ( nargchk ( 0, 1, nargin ) );
%   error ( nargoutchk ( 0, 0, nargout ) );
% 
%   % Default value for the output filename
%   if nargin < 1 
%     JacobianRefPointFileName = 'JacobianRefPointMx.cpp';
%   end

  if ~exist ( 'JacobianRefPointFileName', 'var' );
    JacobianRefPointFileName = 'UVL_OPT_jacobianRefPointMx.cpp';
  end

  % Flags for simplifying equations
  DoSimplifyw = false;
  DoSimplifyAbsoluteHomograhies = false;
  DoSimplifyCosts = false;
  DoSimplifyJacobian = false;

  % Symbolic Quaternion Fields for Image i
  vxi = sym ( 'vxi', 'real' );
  vyi = sym ( 'vyi', 'real' );
  vzi = sym ( 'vzi', 'real' );
  vwi = (1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2);
  % Scalar from vector
  if DoSimplifyw;
    wi = vwi;
  else
    wi = sym ( 'wi', 'real' );
  end

  % Symbolic Translation Parameters for Image i
  txi = sym ( 'txi', 'real' );
  tyi = sym ( 'tyi', 'real' );
  tzi = sym ( 'tzi', 'real' );

  % Symbolic Points and Matches
  px = sym ( 'px', 'real' );
  py = sym ( 'py', 'real' );
  mx = sym ( 'mx', 'real' );
  my = sym ( 'my', 'real' );

  % Symbolic Camera Parameters
  au = sym ( 'au', 'real' );
  av = sym ( 'av', 'real' );
  cx = sym ( 'cx', 'real' );
  cy = sym ( 'cy', 'real' );

  % Symblic Camera Intrinsic Parameters
  K = [ au   0  cx;
         0  av  cy;
         0   0   1 ];

  % Symblic Elements of Rotation Matrices Represented by Quaternions for Image i
  Ri = UVL_OPT_quaternionToRotationMatrix ( [ wi vxi vyi vzi ] );

  % Translation for Factorized Projection Matrix 
  Ti = [1 0 -txi; 0 1 -tyi; 0 0 -tzi];

  % Compute iHw = K * cRw * wTc
  iHw = K * Ri * Ti;

  if DoSimplifyAbsoluteHomograhies;
    disp ( 'Simplifying Absolute Homograhpy Expressions...' );
    iHw = simplify ( iHw );
  end

  % Residuals from Reference Point Readings
  %   px, py: Imaged Point coordinates (Image Reference Frame)
  %   mx, my: Reference Point coordinates (Mosaic Reference Frame)
  disp ( 'Calculating Residuals for Reference (Fiducial) Point Readings (x and y)...' );
  CostRefPoint_x = px - (iHw(1,:)*[mx;my;1]) / (iHw(3,:)*[mx;my;1]);
  CostRefPoint_y = py - (iHw(2,:)*[mx;my;1]) / (iHw(3,:)*[mx;my;1]);

  if DoSimplifyCosts;
    disp ( 'Simplifying Cost Expressions...' );
    sCostRefPoint_x = simplify ( CostRefPoint_x );
    sCostRefPoint_y = simplify ( CostRefPoint_y );
  else
    sCostRefPoint_x = CostRefPoint_x;
    sCostRefPoint_y = CostRefPoint_y;
  end

  % Sizes
  NumJac = 2;
  NumVars = 6;
  JacNames = cell ( NumJac, 1 );
  JacNames{1} = 'x';
  JacNames{2} = 'y';

  % Allocate Cell-Array
  dCostRefPoint = cell ( NumJac, 1 );
  dsCostRefPoint = cell ( NumJac, 1 );

  % Replace wi and wj for its value
  if ~DoSimplifyw;
    sCostRefPoint_x = subs ( sCostRefPoint_x, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
    sCostRefPoint_y = subs ( sCostRefPoint_y, wi, ( 1.0 - vxi * vxi - vyi * vyi - vzi * vzi )^(1/2) );
  end

  % Jacobian for Reference (Fiducial) Point Readings
  disp ( 'Calculating Jacobian for x...' );
  dCostRefPoint{1} = jacobian ( sCostRefPoint_x, [vxi, vyi, vzi, txi, tyi, tzi] );

  disp ( 'Calculating Jacobian for y...' );
  dCostRefPoint{2} = jacobian ( sCostRefPoint_y, [vxi, vyi, vzi, txi, tyi, tzi] );

  if DoSimplifyJacobian;
    for j = 1 : NumJac;
      fprintf ( 'Simplifying Jacobian for %s...\n', JacNames{j} );
      for i = 1 : NumVars;
        fprintf ( '  Simplifying Jacobian %d/%d\n', i, NumVars );
        dsCostRefPoint{j}(1,i) = simplify ( dCostRefPoint{j}(1,i) );
      end
    end
  else
    for j = 1 : NumJac;
      dsCostRefPoint{j} = dCostRefPoint{j};
    end
  end

  % Substitutions
  Subs = cell ( 2, 0 );
  i = 1;
  Subs{1,i} = 'vxi*vxi';                    Subs{2,i} = 'vxi2';  i = i + 1;
  Subs{1,i} = 'vyi*vyi*vyi*vyi';            Subs{2,i} = 'vyi4';  i = i + 1;
  Subs{1,i} = 'vyi*vyi';                    Subs{2,i} = 'vyi2';  i = i + 1;
  Subs{1,i} = 'vzi*vzi*vzi';                Subs{2,i} = 'vzi3';  i = i + 1;
  Subs{1,i} = 'vzi*vzi';                    Subs{2,i} = 'vzi2';  i = i + 1;
  Subs{1,i} = '-vxi2-vyi2-vzi2';            Subs{2,i} = 'wi2';   i = i + 1;
  Subs{1,i} = 'sqrt(wi2+1.0)';              Subs{2,i} = 'wi';    i = i + 1;

  Subs{1,i} = 'vxi*vyi';                    Subs{2,i} = 'vxyi';  i = i + 1;
  Subs{1,i} = 'vxi*vzi';                    Subs{2,i} = 'vxzi';  i = i + 1;
  Subs{1,i} = 'vyi*vzi';                    Subs{2,i} = 'vyzi';  i = i + 1;

  % Create the output Mex file
  FId = fopen ( JacobianRefPointFileName, 'w' );
  if FId ~= -1;
    fprintf ( 'Generating "%s" Mex File...\n', JacobianRefPointFileName );

    fprintf ( FId, '//\n' );
    fprintf ( FId, '// C++ Automatically generated file using ''%s.m''\n', mfilename );
    fprintf ( FId, '// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>\n' );
    fprintf ( FId, '// Date: %s\n', datestr ( now ) );
    fprintf ( FId, '//\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#include <mex.h>          // Matlab Mex Functions\n' );
    fprintf ( FId, '#include <cmath>\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#define ERROR_HEADER       "MATLAB:%s:Input\\n"\n', JacobianRefPointFileName );
    fprintf ( FId, '#define VARIABLES          "Vars = (au, av, cx, cy, vxi, vyi, vzi, txi, tyi, tzi, mx, my)"\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify the input parameters by it''s index\n' );
    fprintf ( FId, 'enum { VARS=0 };\n' );

    fprintf ( FId, '// Identify the output parameters by it''s index\n' );
    fprintf ( FId, 'enum { J1=0, J2 };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify elements in Vars vector by it''s index\n' );
    fprintf ( FId, 'enum { AU=0, AV, CX, CY, VXI, VYI, VZI, TXI, TYI, TZI, MX, MY };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, 'void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )\n' );
    fprintf ( FId, '{\n' );
    fprintf ( FId, '  // Parameter checks\n');
    fprintf ( FId, '  if ( nlhs != 2 || nrhs != 1 )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Usage: [j1, j2] = %s ( Vars )\\n\\n"\n', JacobianRefPointFileName );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  int M = mxGetM ( prhs[VARS] );\n' );
    fprintf ( FId, '  int N = mxGetN ( prhs[VARS] );\n' );
    fprintf ( FId, '  if ( ( M != 1 || N != 12 ) && ( M != 12 || N != 1 ) )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Vars must be a 1x12 or a 12x1 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *Vars = (double *)mxGetPr ( prhs[VARS] );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double au  = Vars[AU];\n' );
    fprintf ( FId, '  double av  = Vars[AV];\n' );
    fprintf ( FId, '  double cx  = Vars[CX];\n' );
    fprintf ( FId, '  double cy  = Vars[CY];\n' );
    fprintf ( FId, '  double vxi = Vars[VXI];\n' );
    fprintf ( FId, '  double vyi = Vars[VYI];\n' );
    fprintf ( FId, '  double vzi = Vars[VZI];\n' );
    fprintf ( FId, '  double txi = Vars[TXI];\n' );
    fprintf ( FId, '  double tyi = Vars[TYI];\n' );
    fprintf ( FId, '  double tzi = Vars[TZI];\n' );
    fprintf ( FId, '  double mx  = Vars[MX];\n' );
    fprintf ( FId, '  double my  = Vars[MY];\n' );
    fprintf ( FId, '\n' );

    % Perform substitutions
    for i = 1 : size ( Subs, 2 );
      fprintf ( FId, '  double %s = %s;\n', Subs{2,i}, Subs{1,i} );
    end
    fprintf ( FId, '\n' );

    % Check for the negative square root
    fprintf ( FId, '  // Check whether the sqrt() can be computed or not (we have a right vector part of the quaternion!)\n' );
    fprintf ( FId, '  if ( wi2 < -1.0 )\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    mexPrintf ( "Point Matches Jacobian cannot be calculated: (wi2 = %%f) < 0 in sqrt (wi2)!\\n", wi2 );\n' );
    fprintf ( FId, '    wi = 0.0;\n' );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '\n' );

    % Write Jacobian
    for i = 1 : NumJac;
      UVL_OPT_generateJacobianSubsStr ( FId, dsCostRefPoint{i}, JacNames{i}, Subs, i, NumVars );
    end

    fprintf ( FId, '}\n' );

    fclose ( FId );
  else
    fprintf ( 'Error writing file "%s"\n', JacobianRefPointFileName );
  end

% end
