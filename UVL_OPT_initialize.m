fprintf ( 'Setting Paths...\n' );

% Check if 3DMotion 1.0 and GMML 1.2 are present
Dependences = { '3DMotion', 'GMML' };

for i = 1 : numel ( Dependences );
  Project = Dependences{i};
  fprintf ( 'Checking for project ''%s''...', Project );
  if isempty ( strfind ( path, [ '\' Project ] ) ) && isempty ( strfind ( path, [ '/' Project ] ) );
      fprintf ( 'Error!\n' );
      warning ( 'MATLAB:UVL_OPT_initialize:Check', 'It seems that ''%s'' project is not in the path!', Project);
      disp ( 'Press CTRL+C to abort and solve or RETURN to continue' );
      pause;
  else
      fprintf ( 'Ok!\n' );
  end
end

% Set the path
path ( [cd '/Optimization'], path );
path ( [cd '/Symbolic'], path );
path ( [cd '/Plotting'], path );
path ( cd, path );
savepath



% fprintf ( '\nGenerating Symbolic Cost Functions and Jacobian...\n' );
%
% DoGenerateJacobian = true;
% DoCompileJacobian = false;
% cd Symbolic
% UVL_OPT_compile
% cd ..



fprintf ( '\nCompiling Symbolic Cost Functions and Jacobian...\n' );

DoGenerateJacobian = false;
DoCompileJacobian = true;
cd Symbolic
UVL_OPT_compile
cd ..    
clear
